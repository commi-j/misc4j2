package tk.labyrinth.misc4j2.lib.junit5;

import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;

class FileAssertionsTest {

	@Test
	void testAssertContentEquals() throws URISyntaxException {
		FileAssertions.assertContentEquals(
				Path.of(ClassLoader.getSystemResource("aDirectory").toURI()),
				Path.of(ClassLoader.getSystemResource("bDirectory").toURI()));
		ContribAssertions.assertThrows(
				() -> FileAssertions.assertContentEquals(
						Path.of(ClassLoader.getSystemResource("aDirectory").toURI()),
						Path.of(ClassLoader.getSystemResource("cDirectory").toURI())),
				fault -> ContribAssertions.assertEndsWith(
						"offset = 3",
						fault.getMessage()));
	}
}