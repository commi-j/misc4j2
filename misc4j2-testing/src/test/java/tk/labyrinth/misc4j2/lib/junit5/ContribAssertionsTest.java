package tk.labyrinth.misc4j2.lib.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

class ContribAssertionsTest {

	@Test
	void testAssertEndsWith() {
		Assertions.assertDoesNotThrow(() -> {
			ContribAssertions.assertEndsWith("", "");
			ContribAssertions.assertEndsWith("a", "a");
			ContribAssertions.assertEndsWith("b", "ab");
		});
		//
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEndsWith("a", "b"), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("endsWith:\nExpected: a\nActual: b", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEndsWith("a", null), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("endsWith:\nExpected: a\nActual: null", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEndsWith(null, "a"), fault -> {
			Assertions.assertEquals(NullPointerException.class, fault.getClass());
			Assertions.assertEquals("expected", fault.getMessage());
		});
	}

	@Test
	void testAssertEqualsWithListAndStream() {
		Assertions.assertDoesNotThrow(() -> {
			ContribAssertions.assertEquals(List.of(), Stream.empty());
			ContribAssertions.assertEquals(List.of("a"), Stream.of("a"));
			ContribAssertions.assertEquals(List.of("a", "b"), Stream.of("a", "b"));
			ContribAssertions.assertEquals((List<?>) null, null);
		});
		//
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals(List.of("a"), Stream.of("b")), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <[a]> but was: <[b]>", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals(List.of("a"), null), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <[a]> but was: <null>", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals((List<String>) null, Stream.of("b")), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <null> but was: <[b]>", fault.getMessage());
		});
	}

	@Test
	void testAssertEqualsWithSetAndStream() {
		Assertions.assertDoesNotThrow(() -> {
			ContribAssertions.assertEquals(Set.of(), Stream.empty());
			ContribAssertions.assertEquals(Set.of("a"), Stream.of("a"));
			ContribAssertions.assertEquals(Set.of("a", "b"), Stream.of("a", "b"));
			ContribAssertions.assertEquals((Set<?>) null, null);
		});
		//
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals(Set.of("a"), Stream.of("b")), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <[a]> but was: <[b]>", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals(Set.of("a"), null), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <[a]> but was: <null>", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertEquals((Set<String>) null, Stream.of("b")), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("expected: <null> but was: <[b]>", fault.getMessage());
		});
	}

	@Test
	void testAssertStartsWith() {
		Assertions.assertDoesNotThrow(() -> {
			ContribAssertions.assertStartsWith("", "");
			ContribAssertions.assertStartsWith("a", "a");
			ContribAssertions.assertStartsWith("a", "ab");
		});
		//
		ContribAssertions.assertThrows(() -> ContribAssertions.assertStartsWith("a", "b"), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("startsWith:\nExpected: a\nActual: b", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertStartsWith("a", null), fault -> {
			Assertions.assertEquals(AssertionFailedError.class, fault.getClass());
			Assertions.assertEquals("startsWith:\nExpected: a\nActual: null", fault.getMessage());
		});
		ContribAssertions.assertThrows(() -> ContribAssertions.assertStartsWith(null, "a"), fault -> {
			Assertions.assertEquals(NullPointerException.class, fault.getClass());
			Assertions.assertEquals("expected", fault.getMessage());
		});
	}
}
