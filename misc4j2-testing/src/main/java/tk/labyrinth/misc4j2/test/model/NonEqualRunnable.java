package tk.labyrinth.misc4j2.test.model;

public class NonEqualRunnable implements Runnable {

	@Override
	public boolean equals(Object obj) {
		return false;
	}

	@Override
	public void run() {
		// no-op
	}
}
