package tk.labyrinth.misc4j2.test.meta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class MetaAnnotationTestBase<A extends Annotation> {

	@Test
	void testAnnotationTypes() {
		getAnnotationTypes().forEach(annotationType ->
				Assertions.assertTrue(getType().isAnnotationPresent(annotationType)));
	}

	@Test
	void testDocumented() {
		Assertions.assertEquals(isDocumented(), getType().isAnnotationPresent(Documented.class));
	}

	@Test
	void testRetention() {
		Retention retention = getType().getAnnotation(Retention.class);
		Assertions.assertNotNull(retention);
		Assertions.assertEquals(getRetentionPolicy(), retention.value());
	}

	@Test
	void testTarget() {
		Target target = getType().getAnnotation(Target.class);
		Assertions.assertNotNull(target);
		Assertions.assertEquals(getElementTypes().collect(Collectors.toList()),
				Arrays.asList(target.value()));
	}

	protected Stream<Class<? extends Annotation>> getAnnotationTypes() {
		return Stream.empty();
	}

	protected abstract Stream<ElementType> getElementTypes();

	protected RetentionPolicy getRetentionPolicy() {
		return RetentionPolicy.RUNTIME;
	}

	protected abstract Class<A> getType();

	/**
	 * Overridable method to specify whether tested annotation must be {@link Documented @Documented}.
	 *
	 * @return whether this annotation should be documented
	 */
	protected boolean isDocumented() {
		return true;
	}
}
