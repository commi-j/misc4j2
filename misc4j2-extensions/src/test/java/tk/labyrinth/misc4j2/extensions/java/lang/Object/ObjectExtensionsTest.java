package tk.labyrinth.misc4j2.extensions.java.lang.Object;

import org.junit.jupiter.api.Test;

class ObjectExtensionsTest {

	@Test
	void testLet() {
		String s = "hello";
		//
		// Source code.
//		s.let(it -> {
//			System.out.println(it.length());
//			return it;
//		});
		//
		// Expected compiled (decompiled) code.
//		ObjectExtensions.let(s, it -> {
//			System.out.println(it.length());
//			return it;
//		});
		//
//		BigInteger bi = BigInteger.TEN;
//		bi.let(it -> {
////			System.out.println(it.nextProbablePrime());
//			return it;
//		});
	}

	@Test
	void testLetNullable() {
		// TODO
	}
}
