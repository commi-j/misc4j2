package tk.labyrinth.misc4j2.extensions.java.io.File;
//import manifold.ext.api.Extension;
//import manifold.ext.api.This;

//@Extension
public class FileExtensions {
	/**
	 * Returns child File of this.
	 *
	 * @param it   non-null
	 * @param name fit
	 *
	 * @return non-null
	 */
//	public static File child(@This File it, String name) {
//		Objects.requireNonNull(it, "it");
//		// TODO: Require fit.
//		Objects.requireNonNull(name, "name");
//		return FileUtils.child(it, name);
//	}
	/**
	 * Returns the extension of this.
	 * <pre>
	 * "wat"      -&gt; null
	 * "wat."     -&gt; ""
	 * "wat.java" -&gt; "java"
	 * </pre>
	 *
	 * @param it non-null
	 *
	 * @return nullable
	 */
//	@Nullable
//	public static String getExtension(@This File it) {
//		Objects.requireNonNull(it, "it");
//		return FileUtils.getExtension(it);
//	}
}
