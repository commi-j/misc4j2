//
group = "tk.labyrinth"
version = "0.1.0-SNAPSHOT"
//
plugins {
	`java-library`
}
//
allprojects {
	apply {
		`java-library`
	}
	//
	repositories {
		mavenCentral()
		mavenLocal()
	}
	//
	dependencies {
		//
	}
	//
	tasks {
		val versionJava = "11"
		//
		compileJava {
			sourceCompatibility = versionJava
			targetCompatibility = versionJava
		}
		//
		test {
			useJUnitPlatform()
		}
	}
}
//
// Utility
inline val ObjectConfigurationAction.`java-library`: ObjectConfigurationAction
	get() = plugin("java-library")
