package tk.labyrinth.misc4j2.io.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.java.io.FileUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.io.File;

class FileUtilsTest {

	@Test
	void testChild() {
		Assertions.assertNull(FileUtils.child(null, "child"));
		Assertions.assertEquals(new File("parent/child"), FileUtils.child(new File("parent"), "child"));
	}

	@Test
	void testGetBaseName() {
		Assertions.assertEquals("file", FileUtils.getBaseName(new File("file")));
		Assertions.assertEquals("file", FileUtils.getBaseName(new File("file.")));
		Assertions.assertEquals("file", FileUtils.getBaseName(new File("file.io")));
		Assertions.assertEquals("", FileUtils.getBaseName(new File(".io")));
		Assertions.assertEquals("file.io", FileUtils.getBaseName(new File("file.io.nio")));
		//
		ContribAssertions.assertThrows(
				() -> FileUtils.getBaseName(null),
				fault -> Assertions.assertEquals("java.lang.NullPointerException: file", fault.toString()));
	}

	@Test
	void testGetExtension() {
		Assertions.assertNull(FileUtils.getExtension(new File("file")));
		Assertions.assertEquals("", FileUtils.getExtension(new File("file.")));
		Assertions.assertEquals("io", FileUtils.getExtension(new File("file.io")));
		Assertions.assertEquals("nio", FileUtils.getExtension(new File("file.io.nio")));
		//
		ContribAssertions.assertThrows(
				() -> FileUtils.getExtension(null),
				fault -> Assertions.assertEquals("java.lang.NullPointerException: file", fault.toString()));
	}
}
