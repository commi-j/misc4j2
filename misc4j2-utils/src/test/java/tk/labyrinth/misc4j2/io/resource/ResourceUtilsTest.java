package tk.labyrinth.misc4j2.io.resource;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.java.io.ResourceUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

class ResourceUtilsTest {

	@Test
	void testGetResourceUrls() throws IOException {
		List<URL> resourceUrls = ResourceUtils.getResourceUrls("test/test-resource")
				.collect(Collectors.toList());
		{
			URL url = resourceUrls.get(0);
			ContribAssertions.assertEndsWith("/misc4j2-utils/target/test-classes/test/test-resource", url.toString());
			Assertions.assertEquals("test", IOUtils.toString(url, StandardCharsets.UTF_8).trim());
		}
		{
			URL url = resourceUrls.get(1);
			ContribAssertions.assertEndsWith("/misc4j2-utils/target/classes/test/test-resource", url.toString());
			Assertions.assertEquals("main", IOUtils.toString(url, StandardCharsets.UTF_8).trim());
		}
	}
}
