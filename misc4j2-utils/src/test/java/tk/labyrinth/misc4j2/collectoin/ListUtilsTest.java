package tk.labyrinth.misc4j2.collectoin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class ListUtilsTest {

	@Test
	void testCollectWithObjectAndStream() {
		Assertions.assertEquals(List.of("a"), ListUtils.collect("a", Stream.empty()));
		Assertions.assertEquals(List.of("a", "b"), ListUtils.collect("a", Stream.of("b")));
		Assertions.assertEquals(Arrays.asList(null, "b"), ListUtils.collect(null, Stream.of("b")));
		//
		//noinspection ConstantConditions
		ContribAssertions.assertThrows(() -> ListUtils.collect("a", null), fault -> {
			Assertions.assertEquals(NullPointerException.class, fault.getClass());
			Assertions.assertEquals("second", fault.getMessage());
		});
	}

	@Test
	void testFromEnumeration() {
		// TODO
	}

	@Test
	void testFromIterator() {
		// TODO
	}
}
