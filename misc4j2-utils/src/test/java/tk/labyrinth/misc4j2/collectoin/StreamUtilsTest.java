package tk.labyrinth.misc4j2.collectoin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class StreamUtilsTest {

	@Test
	void testConcatWithObjectAndStream() {
		ContribAssertions.assertEquals(List.of("a"), StreamUtils.concat("a", Stream.empty()));
		ContribAssertions.assertEquals(List.of("a", "b"), StreamUtils.concat("a", Stream.of("b")));
		ContribAssertions.assertEquals(Arrays.asList(null, "b"), StreamUtils.concat((String) null, Stream.of("b")));
		//
		//noinspection ConstantConditions
		ContribAssertions.assertThrows(() -> StreamUtils.concat("a", (Stream<String>) null), fault -> {
			Assertions.assertEquals(NullPointerException.class, fault.getClass());
			Assertions.assertEquals("second", fault.getMessage());
		});
	}

	/**
	 * Third argument is only applied for parallel streams.
	 */
	@Test
	void testStreamReduceWithThreeArguments() {
		Assertions.assertEquals(
				10,
				Stream.of("1", "2", "3").reduce(
						4,
						(first, second) -> first + Integer.parseInt(second),
						(first, second) -> {
							throw new UnsupportedOperationException();
						}));
	}

	public static void qwe() {
		throw new NotImplementedException();
	}
}
