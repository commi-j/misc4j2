package tk.labyrinth.misc4j2.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.time.LocalDate;
import java.time.Period;

class LocalDateRangeTest {

	@Test
	void testContains() {
		LocalDate ld0 = LocalDate.of(1, 1, 1);
		LocalDate ld1 = LocalDate.of(2, 2, 2);
		LocalDate ld2 = LocalDate.of(3, 3, 3);
		LocalDate ld3 = LocalDate.of(4, 4, 4);
		LocalDate ld4 = LocalDate.of(5, 5, 5);
		{
			LocalDateRange range = LocalDateRange.ofFinite(ld1, ld3);
			Assertions.assertFalse(range.contains(ld0));
			Assertions.assertTrue(range.contains(ld1));
			Assertions.assertTrue(range.contains(ld2));
			Assertions.assertTrue(range.contains(ld3));
			Assertions.assertFalse(range.contains(ld4));
		}
		{
			LocalDateRange range = LocalDateRange.ofInfiniteFrom(ld3);
			Assertions.assertTrue(range.contains(ld0));
			Assertions.assertTrue(range.contains(ld1));
			Assertions.assertTrue(range.contains(ld2));
			Assertions.assertTrue(range.contains(ld3));
			Assertions.assertFalse(range.contains(ld4));
		}
		{
			LocalDateRange range = LocalDateRange.ofInfiniteTo(ld1);
			Assertions.assertFalse(range.contains(ld0));
			Assertions.assertTrue(range.contains(ld1));
			Assertions.assertTrue(range.contains(ld2));
			Assertions.assertTrue(range.contains(ld3));
			Assertions.assertTrue(range.contains(ld4));
		}
	}

	@Test
	void testGetPeriod() {
		Assertions.assertEquals(Period.ofDays(1), LocalDateRange.ofFinite(
				LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")).getPeriod());
		Assertions.assertEquals(Period.ofDays(2), LocalDateRange.ofFinite(
				LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")).getPeriod());
		Assertions.assertEquals(Period.of(0, 1, 1), LocalDateRange.ofFinite(
				LocalDate.parse("2020-04-04"), LocalDate.parse("2020-05-04")).getPeriod());
	}

	@Test
	void testOfFinite() {
		Assertions.assertEquals("LocalDateRange{from=2020-04-04, to=2020-04-04}", LocalDateRange.ofFinite(
				LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")).toString());
		Assertions.assertEquals("LocalDateRange{from=2020-04-04, to=2020-04-05}", LocalDateRange.ofFinite(
				LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")).toString());
		//
		ContribAssertions.assertThrows(() -> LocalDateRange.ofFinite(LocalDate.parse("2020-04-05"), LocalDate.parse("2020-04-04")), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("From is before to: from = 2020-04-05, to = 2020-04-04", fault.getMessage());
		});
		//noinspection ConstantConditions
		ContribAssertions.assertThrows(() -> LocalDateRange.ofFinite(null, LocalDate.parse("2020-04-04")), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("Require finite: LocalDateRange{from=inf, to=2020-04-04}", fault.getMessage());
		});
		//noinspection ConstantConditions
		ContribAssertions.assertThrows(() -> LocalDateRange.ofFinite(LocalDate.parse("2020-04-04"), null), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("Require finite: LocalDateRange{from=2020-04-04, to=inf}", fault.getMessage());
		});
	}

	@Test
	void testOfInfiniteFrom() {
		LocalDate localDate = LocalDate.parse("2020-04-04");
		LocalDateRange range = LocalDateRange.ofInfiniteFrom(localDate);
		//
		Assertions.assertNull(range.getFrom());
		Assertions.assertEquals(localDate, range.getTo());
	}

	@Test
	void testOfInfiniteTo() {
		LocalDate localDate = LocalDate.parse("2020-04-04");
		LocalDateRange range = LocalDateRange.ofInfiniteTo(localDate);
		//
		Assertions.assertEquals(localDate, range.getFrom());
		Assertions.assertNull(range.getTo());
	}
}
