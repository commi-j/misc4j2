package tk.labyrinth.misc4j2.ioc.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class SimpleBeanContextTest {

	@Test
	void testSimpleCase() {
		SimpleBeanContext context = new SimpleBeanContext();
		context.registerInstance(new ArrayList<>());
		//
		Assertions.assertEquals(ArrayList.class, context.get(List.class).getClass());
	}
}
