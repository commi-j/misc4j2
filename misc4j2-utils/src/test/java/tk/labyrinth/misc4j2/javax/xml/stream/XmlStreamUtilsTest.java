package tk.labyrinth.misc4j2.javax.xml.stream;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

@ExtendWith(SpringExtension.class)
@RequiredArgsConstructor
class XmlStreamUtilsTest {

	@Autowired
	private ResourceLoader resourceLoader;

	@Test
	void testFindNextElementWithName() throws IOException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(
				resourceLoader.getResource("cd_catalog.xml").getInputStream());
		//
		int foundCount = 0;
		for (int i = 0; i < 30; i++) {
			if (XmlStreamUtils.findNextElementWithName(xmlEventReader, "CD")) {
				xmlEventReader.nextEvent();
				foundCount++;
			}
		}
		Assertions.assertEquals(26, foundCount);
	}

	@Test
	void testReadElementAsString() throws IOException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(
				resourceLoader.getResource("cd_catalog.xml").getInputStream());
		//
		{
			XmlStreamUtils.findNextElementWithName(xmlEventReader, "CD");
			Assertions.assertEquals(
					"""
							<CD>
									<TITLE>Empire Burlesque</TITLE>
									<ARTIST>Bob Dylan</ARTIST>
									<COUNTRY>USA</COUNTRY>
									<COMPANY>Columbia</COMPANY>
									<PRICE>10.90</PRICE>
									<YEAR>1985</YEAR>
								</CD>""",
					XmlStreamUtils.readNextElementAsString(xmlEventReader));
		}
		{
			XmlStreamUtils.findNextElementWithName(xmlEventReader, "CD");
			Assertions.assertEquals(
					"""
							<CD>
									<TITLE>Hide your heart</TITLE>
									<ARTIST>Bonnie Tyler</ARTIST>
									<COUNTRY>UK</COUNTRY>
									<COMPANY>CBS Records</COMPANY>
									<PRICE>9.90</PRICE>
									<YEAR>1988</YEAR>
								</CD>""",
					XmlStreamUtils.readNextElementAsString(xmlEventReader));
		}
	}

	@Test
	void testReadElementAsStringWhenCursorIsAtNull() throws IOException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(
				resourceLoader.getResource("cd_catalog.xml").getInputStream());
		//
		XmlStreamUtils.findNextElementWithName(xmlEventReader, "no_name");
		ContribAssertions.assertThrows(
				() -> XmlStreamUtils.readNextElementAsString(xmlEventReader),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Require firstEvent to be startElement: null",
						fault.toString()));
	}

	@Test
	void testReadElementAsStringWhenCursorIsAtStartDocument() throws IOException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(
				resourceLoader.getResource("cd_catalog.xml").getInputStream());
		//
		ContribAssertions.assertThrows(
				() -> XmlStreamUtils.readNextElementAsString(xmlEventReader),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Require firstEvent to be startElement: [Stax Event #7]",
						fault.toString()));
	}
}