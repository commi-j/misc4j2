package tk.labyrinth.misc4j2.common.merge;

import lombok.Value;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.Nullable;
import java.util.function.BinaryOperator;

class MergeUtilsTest {

	@Test
	void testMergeEqual() {
		Assertions.assertEquals(
				"TEST",
				MergeUtils.mergeEqual(
						new TestValue("TEST"),
						new TestValue("TEST"),
						TestValue::getVal,
						"val"));
		Assertions.assertEquals(
				"FIRST",
				MergeUtils.mergeEqual(
						new TestValue("FIRST"),
						new TestValue(null),
						TestValue::getVal,
						"val"));
		Assertions.assertEquals(
				"SECOND",
				MergeUtils.mergeEqual(
						new TestValue(null),
						new TestValue("SECOND"),
						TestValue::getVal,
						"val"));
		ContribAssertions.assertThrows(
				() -> MergeUtils.mergeEqual(
						new TestValue("FIRST"),
						new TestValue("SECOND"),
						TestValue::getVal,
						"val"),
				fault -> {
					Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
					Assertions.assertEquals(
							"Require equal val: first = MergeUtilsTest.TestValue(val=FIRST), second = MergeUtilsTest.TestValue(val=SECOND)",
							fault.getMessage());
				});
	}

	@Test
	void testMergeWithOperator() {
		BinaryOperator<String> propertyOperator = (first, second) -> first + "_" + second;
		//
		Assertions.assertEquals(
				"FIRST_SECOND",
				MergeUtils.mergeWithOperator(
						new TestValue("FIRST"),
						new TestValue("SECOND"),
						TestValue::getVal,
						propertyOperator));
		Assertions.assertEquals(
				"FIRST",
				MergeUtils.mergeWithOperator(
						new TestValue("FIRST"),
						new TestValue(null),
						TestValue::getVal,
						propertyOperator));
		Assertions.assertEquals(
				"SECOND",
				MergeUtils.mergeWithOperator(
						new TestValue(null),
						new TestValue("SECOND"),
						TestValue::getVal,
						propertyOperator));
	}

	@Value
	private static class TestValue {

		@Nullable
		String val;
	}
}
