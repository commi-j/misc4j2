package tk.labyrinth.misc4j2.lang;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;

class ObjectUtilsTest {

	@Test
	void testMergeNullable() {
		BinaryOperator<String> mergeOperator = (first, second) -> first + "." + second;
		Supplier<String> defaultValueSupplier = () -> "nil";
		//
		Assertions.assertEquals("one.two", ObjectUtils.mergeNullable(
				"one", "two", mergeOperator, defaultValueSupplier));
		Assertions.assertEquals("one", ObjectUtils.mergeNullable(
				"one", null, mergeOperator, defaultValueSupplier));
		Assertions.assertEquals("two", ObjectUtils.mergeNullable(
				null, "two", mergeOperator, defaultValueSupplier));
		Assertions.assertEquals("nil", ObjectUtils.mergeNullable(
				null, null, mergeOperator, defaultValueSupplier));
	}

	@Test
	void testPickFirstNonDefaultValue() {
		AtomicInteger counter = new AtomicInteger();
		String value = ObjectUtils.pickFirstNonDefaultValue(
				"",
				() -> {
					counter.incrementAndGet();
					return "foo";
				},
				() -> {
					counter.incrementAndGet();
					return "bar";
				});
		//
		Assertions.assertEquals("foo", value);
		Assertions.assertEquals(1, counter.get());
	}

	@Test
	void testReduce() {
		{
			AtomicInteger iterationCounter = new AtomicInteger(0);
			Integer result = ObjectUtils.reduce(3, value -> value > 0, value -> {
				iterationCounter.incrementAndGet();
				return value - 1;
			});
			//
			Assertions.assertEquals(0, result);
			Assertions.assertEquals(3, iterationCounter.get());
		}
		{
			AtomicInteger iterationCounter = new AtomicInteger(0);
			Integer result = ObjectUtils.reduce(-1, value -> value > 0, value -> {
				iterationCounter.incrementAndGet();
				return value - 1;
			});
			//
			Assertions.assertEquals(-1, result);
			Assertions.assertEquals(0, iterationCounter.get());
		}
	}
}
