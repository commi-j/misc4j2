package tk.labyrinth.misc4j2.java.util.regex;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class RegexesTest {

	@Test
	void testNewline() {
		{
			// Breaks.
			//
			Assertions.assertEquals(
					List.of("a", "b"),
					List.of("a\nb".split(Regexes.newline())));
			Assertions.assertEquals(
					List.of("a", "b"),
					List.of("a\r\nb".split(Regexes.newline())));
		}
		{
			// Does not break.
			//
			Assertions.assertEquals(
					List.of("a\rb"),
					List.of("a\rb".split(Regexes.newline())));
		}
	}
}
