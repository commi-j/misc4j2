package tk.labyrinth.misc4j2.java.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

class LazyTest {

	@Test
	void testSimpleUsage() {
		Lazy<Integer> twelveSquare = Lazy.of(() -> 12 * 12);
		Assertions.assertEquals(144, twelveSquare.get());
	}

	@Test
	void testSupplierInvokedOnlyOnce() {
		AtomicInteger counter = new AtomicInteger();
		Lazy<Integer> lazyCounterFirstValue = Lazy.of(counter::incrementAndGet);
		for (int i = 0; i < 10; i++) {
			Assertions.assertEquals(1, lazyCounterFirstValue.get());
		}
	}
}
