package tk.labyrinth.misc4j2.java.lang.reflect;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.AbstractList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

class ClassUtilsTest {

	@Test
	void testFind() {
		Assertions
				.assertThat(ClassUtils.find("java.lang.Object"))
				.isEqualTo(Object.class);
		Assertions
				.assertThat(ClassUtils.find("int"))
				.isEqualTo(int.class);
		Assertions
				.assertThat(ClassUtils.find("void"))
				.isEqualTo(void.class);
	}

	@Test
	void testGetWithNestedClass() {
		{
			// Binary name.
			//
			Assertions
					.assertThat(ClassUtils.get("java.util.Map$Entry"))
					.isEqualTo(Map.Entry.class);
		}
		{
			// Qualified name.
			//
			ContribAssertions.assertThrows(
					() -> ClassUtils.get("java.util.Map.Entry"),
					fault -> Assertions
							.assertThat(fault.toString())
							.isEqualTo("java.lang.IllegalArgumentException: Not found: classBinaryName = java.util.Map.Entry"));
		}
	}

	@Test
	void testIsAbstract() {
		Assertions
				.assertThat(ClassUtils.isAbstract(Object.class))
				.isFalse();
		Assertions
				.assertThat(ClassUtils.isAbstract(List.class))
				.isTrue();
		Assertions
				.assertThat(ClassUtils.isAbstract(AbstractList.class))
				.isTrue();
		Assertions
				.assertThat(ClassUtils.isAbstract(TimeUnit.class))
				.isFalse();
	}
}