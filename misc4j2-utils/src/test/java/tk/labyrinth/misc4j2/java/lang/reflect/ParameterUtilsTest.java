package tk.labyrinth.misc4j2.java.lang.reflect;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ParameterUtilsTest {

	@Test
	void testGetActualParameterWithTypeVariable() {
		Assertions.assertEquals(
				String.class,
				ParameterUtils.getActualParameter(
						TypeUtils.parameterize(HashMap.class, String.class, Number.class),
						Map.class.getTypeParameters()[0]));
		Assertions.assertEquals(
				Number.class,
				ParameterUtils.getActualParameter(
						TypeUtils.parameterize(HashMap.class, String.class, Number.class),
						Map.class.getTypeParameters()[1]));
	}

	@Test
	void testGetAliases() {
		Assertions.assertEquals(
				List.of(
						ArrayList.class.getTypeParameters()[0],
						AbstractList.class.getTypeParameters()[0],
						AbstractCollection.class.getTypeParameters()[0],
						Collection.class.getTypeParameters()[0],
						Iterable.class.getTypeParameters()[0],
						List.class.getTypeParameters()[0]),
				ParameterUtils.getAliases(ArrayList.class.getTypeParameters()[0]));
	}

	@Test
	void testResolveTypeParameter() {
		Assertions.assertEquals(
				// TODO: May be List, but we need to change alias resolution order.
				Collection.class.getTypeParameters()[0],
				ParameterUtils.resolveTypeParameter(AbstractList.class.getTypeParameters()[0], List.class));
		Assertions.assertEquals(
				AbstractList.class.getTypeParameters()[0],
				ParameterUtils.resolveTypeParameter(AbstractList.class.getTypeParameters()[0], AbstractList.class));
		Assertions.assertEquals(
				AbstractList.class.getTypeParameters()[0],
				ParameterUtils.resolveTypeParameter(AbstractList.class.getTypeParameters()[0], ArrayList.class));
	}
}