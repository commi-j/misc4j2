package tk.labyrinth.misc4j2.java.lang.reflect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

class TypeUtilsTest {

	@Test
	void testHasEffectiveUpperBound() {
		org.assertj.core.api.Assertions
				.assertThat(TypeUtils.hasEffectiveUpperBound(org.apache.commons.lang3.reflect.TypeUtils
						.wildcardType()
						.build()))
				.isFalse();
		org.assertj.core.api.Assertions
				.assertThat(TypeUtils.hasEffectiveUpperBound(org.apache.commons.lang3.reflect.TypeUtils
						.wildcardType()
						.withUpperBounds(Object.class)
						.build()))
				.isFalse();
		org.assertj.core.api.Assertions
				.assertThat(TypeUtils.hasEffectiveUpperBound(org.apache.commons.lang3.reflect.TypeUtils
						.wildcardType()
						.withUpperBounds(String.class)
						.build()))
				.isTrue();
		org.assertj.core.api.Assertions
				.assertThat(TypeUtils.hasEffectiveUpperBound(org.apache.commons.lang3.reflect.TypeUtils
						.wildcardType()
						.withUpperBounds(Boolean.class, String.class)
						.build()))
				.isTrue();
	}

	@Test
	void testIsAssignableTo() {
		Assertions.assertFalse(TypeUtils.isAssignableTo(
				MySpecificInterface.class,
				ParameterUtils.getFirstActualParameter(MySpecificWrapper.class, MyWrapperBase.class)));
	}

	@Test
	void testIsAssignableToWithPlainParametizedTypeAgainsWildcardParameterized() {
		Assertions.assertTrue(TypeUtils.isAssignableTo(
				org.apache.commons.lang3.reflect.TypeUtils.parameterize(List.class, Integer.class),
				org.apache.commons.lang3.reflect.TypeUtils.parameterize(
						List.class,
						org.apache.commons.lang3.reflect.TypeUtils.wildcardType().build())));
	}

	@Test
	void testRequireResolvedWithParameterizedWithPlainTypeType() {
		Type type = org.apache.commons.lang3.reflect.TypeUtils.parameterize(Class.class, Object.class);
		Assertions.assertEquals("java.lang.Class<java.lang.Object>", type.toString());
		Assertions.assertEquals(type, TypeUtils.requireResolved(type));
	}

	@Test
	void testRequireResolvedWithParameterizedWithWildcardTypeType() {
		Type type = org.apache.commons.lang3.reflect.TypeUtils.parameterize(
				Class.class,
				org.apache.commons.lang3.reflect.TypeUtils.wildcardType().build());
		Assertions.assertEquals("java.lang.Class<?>", type.toString());
		ContribAssertions.assertThrows(
				() -> TypeUtils.requireResolved(type),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Require resolved: java.lang.Class<?>",
						fault.toString()));
	}

	@Test
	void testRequireResolvedWithPlainType() {
		Assertions.assertEquals(Object.class, TypeUtils.requireResolved(Object.class));
	}

	@Test
	void testRequireResolvedWithRawType() {
		Assertions.assertEquals(Class.class, TypeUtils.requireResolved(Class.class));
	}

	private interface MyInterfaceBase<T> {
		// empty
	}

	private interface MySpecificInterface<T extends Collection<T>> extends MyInterfaceBase<T> {
		// empty
	}

	private interface MySpecificWrapper extends MyWrapperBase<MyInterfaceBase<? extends List<?>>> {
		// empty
	}

	private interface MyWrapperBase<T> {
		// empty
	}
}