package tk.labyrinth.misc4j2.java.math;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class BigDecimalUtilsTest {

	@Test
	void testGt() {
		Assertions.assertTrue(BigDecimalUtils.gt(new BigDecimal("12"), new BigDecimal("11")));
		Assertions.assertFalse(BigDecimalUtils.gt(new BigDecimal("12"), new BigDecimal("12")));
		Assertions.assertFalse(BigDecimalUtils.gt(new BigDecimal("12"), new BigDecimal("13")));
	}

	@Test
	void testGte() {
		Assertions.assertTrue(BigDecimalUtils.gte(new BigDecimal("12"), new BigDecimal("11")));
		Assertions.assertTrue(BigDecimalUtils.gte(new BigDecimal("12"), new BigDecimal("12")));
		Assertions.assertFalse(BigDecimalUtils.gte(new BigDecimal("12"), new BigDecimal("13")));
	}

	@Test
	void testLt() {
		Assertions.assertFalse(BigDecimalUtils.lt(new BigDecimal("12"), new BigDecimal("11")));
		Assertions.assertFalse(BigDecimalUtils.lt(new BigDecimal("12"), new BigDecimal("12")));
		Assertions.assertTrue(BigDecimalUtils.lt(new BigDecimal("12"), new BigDecimal("13")));
	}

	@Test
	void testLte() {
		Assertions.assertFalse(BigDecimalUtils.lte(new BigDecimal("12"), new BigDecimal("11")));
		Assertions.assertTrue(BigDecimalUtils.lte(new BigDecimal("12"), new BigDecimal("12")));
		Assertions.assertTrue(BigDecimalUtils.lte(new BigDecimal("12"), new BigDecimal("13")));
	}
}
