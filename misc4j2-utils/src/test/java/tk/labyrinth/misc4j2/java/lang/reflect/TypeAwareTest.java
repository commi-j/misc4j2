package tk.labyrinth.misc4j2.java.lang.reflect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TypeAwareTest {

	@Test
	void testGetRawParameterType() {
		{
			Class<?> parameterType = TypeAware.getRawParameterClass(StringTypeAware.class);
			Assertions.assertEquals(String.class, parameterType);
		}
		{
			@SuppressWarnings("rawtypes")
			Class<? extends TTypeAware> demiRawType = StringTypeAware.class;
			Assertions.assertEquals(String.class, TypeAware.getRawParameterClass(demiRawType));
		}
	}

	private static class StringTypeAware extends TTypeAware<String> {
		// empty
	}

	private static class TTypeAware<T> implements TypeAware<T> {
		// empty
	}
}
