package tk.labyrinth.misc4j2.java.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class LocalDateUtilsTest {

	@Test
	void testGt() {
		Assertions.assertTrue(LocalDateUtils.gt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-03")));
		Assertions.assertFalse(LocalDateUtils.gt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")));
		Assertions.assertFalse(LocalDateUtils.gt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")));
	}

	@Test
	void testGte() {
		Assertions.assertTrue(LocalDateUtils.gte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-03")));
		Assertions.assertTrue(LocalDateUtils.gte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")));
		Assertions.assertFalse(LocalDateUtils.gte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")));
	}

	@Test
	void testLt() {
		Assertions.assertFalse(LocalDateUtils.lt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-03")));
		Assertions.assertFalse(LocalDateUtils.lt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")));
		Assertions.assertTrue(LocalDateUtils.lt(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")));
	}

	@Test
	void testLte() {
		Assertions.assertFalse(LocalDateUtils.lte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-03")));
		Assertions.assertTrue(LocalDateUtils.lte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-04")));
		Assertions.assertTrue(LocalDateUtils.lte(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")));
	}
}
