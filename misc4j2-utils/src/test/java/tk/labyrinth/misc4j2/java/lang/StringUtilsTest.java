package tk.labyrinth.misc4j2.java.lang;

import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class StringUtilsTest {

	@Test
	void testFindAllIndicesOf() {
		Assertions
				.assertThat(StringUtils.findAllIndicesOf("$foo:$bar", '$'))
				.isEqualTo(List.of(0, 5));
	}

	@Test
	void testIsCharacterEscaped() {
		Assertions
				.assertThat(StringUtils.isCharacterEscaped("$", 0))
				.isFalse();
		Assertions
				.assertThat(StringUtils.isCharacterEscaped("\\$", 1))
				.isTrue();
		Assertions
				.assertThat(StringUtils.isCharacterEscaped("\\\\$", 2))
				.isFalse();
		Assertions
				.assertThat(StringUtils.isCharacterEscaped("\\\\\\$", 3))
				.isTrue();
	}

	@Test
	void testSplitByFirstOccurrence() {
		Assertions
				.assertThat(StringUtils.splitByFirstOccurrence("foo:bar:foo", ":"))
				.isEqualTo(Pair.of("foo", "bar:foo"));
		Assertions
				.assertThat(StringUtils.splitByFirstOccurrence("foo:bar:foo", "_"))
				.isEqualTo(Pair.of("foo:bar:foo", null));
	}

	@Test
	void testSplitByLastOccurrence() {
		Assertions
				.assertThat(StringUtils.splitByLastOccurrence("foo:bar:foo", ":"))
				.isEqualTo(Pair.of("foo:bar", "foo"));
		Assertions
				.assertThat(StringUtils.splitByLastOccurrence("foo:bar:foo", "_"))
				.isEqualTo(Pair.of("foo:bar:foo", null));
	}
}