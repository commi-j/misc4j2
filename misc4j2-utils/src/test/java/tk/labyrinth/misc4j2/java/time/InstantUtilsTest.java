package tk.labyrinth.misc4j2.java.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;

class InstantUtilsTest {

	@Test
	void testPlusMonths() {
		Assertions.assertEquals(
				Instant.parse("2022-02-28T00:00:00Z"),
				InstantUtils.plusMonths(Instant.parse("2022-01-31T00:00:00Z"), 1, ZoneId.of("Europe/Moscow")));
		Assertions.assertEquals(
				Instant.parse("2022-02-28T21:00:00Z"),
				InstantUtils.plusMonths(Instant.parse("2022-01-31T21:00:00Z"), 1, ZoneId.of("Europe/Moscow")));
		//
		Assertions.assertEquals(
				Instant.parse("2022-03-28T20:00:00Z"),
				InstantUtils.plusMonths(Instant.parse("2022-02-28T20:00:00Z"), 1, ZoneId.of("Europe/Moscow")));
		Assertions.assertEquals(
				Instant.parse("2022-03-31T21:00:00Z"),
				InstantUtils.plusMonths(Instant.parse("2022-02-28T21:00:00Z"), 1, ZoneId.of("Europe/Moscow")));
	}
}