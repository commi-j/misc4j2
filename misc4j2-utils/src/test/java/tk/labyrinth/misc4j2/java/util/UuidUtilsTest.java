package tk.labyrinth.misc4j2.java.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class UuidUtilsTest {

    @Test
    void testPatternWithZero() {
        Assertions.assertTrue(UuidUtils.PATTERN.matcher(UuidUtils.ZERO.toString()).matches());
    }

    @Test
    void testUuidToShortString() {
        Assertions.assertEquals(
                "(0)-0f00-0000-000000000000",
                UuidUtils.uuidToShortString(UUID.fromString("00000000-0000-0f00-0000-000000000000")));
        Assertions.assertEquals(
                "(0)-0000f0000000",
                UuidUtils.uuidToShortString(UUID.fromString("00000000-0000-0000-0000-0000f0000000")));
        Assertions.assertEquals(
                "(0)",
                UuidUtils.uuidToShortString(UuidUtils.ZERO));
        Assertions.assertEquals(
                "0f000000-0000-0000-0000-000000000000",
                UuidUtils.uuidToShortString(UUID.fromString("0f000000-0000-0000-0000-000000000000")));
    }

    @Test
    void testZero() {
        Assertions.assertEquals("00000000-0000-0000-0000-000000000000", UuidUtils.ZERO.toString());
    }
}