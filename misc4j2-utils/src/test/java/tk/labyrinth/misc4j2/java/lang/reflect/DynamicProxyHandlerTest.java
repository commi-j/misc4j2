package tk.labyrinth.misc4j2.java.lang.reflect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;
import tk.labyrinth.misc4j2.test.model.NonEqualRunnable;

import java.util.ArrayList;
import java.util.List;

class DynamicProxyHandlerTest {

	@Test
	void testInvoke() {
		List<String> target = new ArrayList<>();
		List<String> proxy;
		{
			DynamicProxyHandler<List<String>> proxyHandler = ObjectUtils.infer(DynamicProxyHandler.ofType(List.class));
			proxyHandler.setTarget(target);
			proxy = proxyHandler.getProxy();
		}
		{
			proxy.add("foo");
			Assertions.assertEquals(List.of("foo"), target);
		}
	}

	/**
	 * Invocation of equals on proxy itself will yield true without invoking target.<br>
	 * <br>
	 * There are two ways of making equals work with proxies on objects which are only equal to themselves:<br>
	 * - Return true for the same proxy identity, which does not work for different proxies of the same object;<br>
	 * - Unwrap proxy and compare real object;<br>
	 * This is the only way we can make equals work properly on objects which are only equal to themselves.<br>
	 * (Actually there is another way of unwrapping proxied objects
	 */
	@Test
	void testInvokeEquals() {
		DynamicProxyHandler<Runnable> proxyHandler = DynamicProxyHandler.ofType(Runnable.class);
		proxyHandler.setTarget(new NonEqualRunnable());
		//
		Assertions.assertEquals(proxyHandler.getProxy(), proxyHandler.getProxy());
		Assertions.assertNotEquals(proxyHandler.getTarget(), proxyHandler.getProxy());
		Assertions.assertNotEquals(proxyHandler.getProxy(), proxyHandler.getTarget());
		Assertions.assertNotEquals(proxyHandler.getTarget(), proxyHandler.getTarget());
	}

	@Test
	void testInvokeIdentityComparison() {
		DynamicProxyHandler<List<?>> proxyHandler = ObjectUtils.infer(DynamicProxyHandler.ofType(List.class));
		proxyHandler.setTarget(new ArrayList<>());
		Assertions.assertSame(proxyHandler.getProxy(), proxyHandler.getProxy());
		Assertions.assertNotSame(proxyHandler.getTarget(), proxyHandler.getProxy());
	}

	@Test
	void testInvokeWithFault() {
		List<String> proxy;
		{
			DynamicProxyHandler<List<String>> proxyHandler = ObjectUtils.infer(DynamicProxyHandler.ofType(List.class));
			proxyHandler.setTarget(List.of());
			proxy = proxyHandler.getProxy();
		}
		{
			ContribAssertions.assertThrows(() -> proxy.add("foo"), fault -> {
				Assertions.assertEquals(UnsupportedOperationException.class, fault.getClass());
				Assertions.assertNull(fault.getMessage());
			});
		}
	}

	@Test
	void testInvokeWithoutTarget() {
		List<String> proxy;
		{
			DynamicProxyHandler<List<String>> proxyHandler = ObjectUtils.infer(DynamicProxyHandler.ofType(List.class));
			proxy = proxyHandler.getProxy();
		}
		{
			ContribAssertions.assertThrows(() -> proxy.add("foo"), fault -> {
				Assertions.assertEquals(IllegalStateException.class, fault.getClass());
				Assertions.assertEquals("target is null", fault.getMessage());
			});
		}
	}
}
