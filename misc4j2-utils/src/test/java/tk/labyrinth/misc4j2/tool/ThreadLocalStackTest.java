package tk.labyrinth.misc4j2.tool;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ThreadLocalStackTest {

	@Test
	void testUpdate() {
		ThreadLocalStack<List<Integer>> threadLocalStack = new ThreadLocalStack<>(true);
		//
		threadLocalStack.push(List.empty());
		threadLocalStack.update(list -> list.append(0));
		//
		Assertions
				.assertThat(threadLocalStack.getStack())
				.isEqualTo(List.of(List.of(0)));
	}
}