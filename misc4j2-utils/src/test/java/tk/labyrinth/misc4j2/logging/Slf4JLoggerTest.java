package tk.labyrinth.misc4j2.logging;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;

import java.util.function.Supplier;

class Slf4JLoggerTest {

	@Test
	void testSupplierObjects() {
		{
			Logger logger = LoggerFactory.getLogger(Slf4JLoggerTest.class);
			logger.info("FOO_0");
			logger.info("FOO_1: {}", "bar");
			logger.info("FOO_2: {}", (Supplier<String>) () -> "bar");
		}
		{
			XLogger logger = XLoggerFactory.getXLogger(Slf4JLoggerTest.class);
			logger.info("FOO_3");
			logger.info("FOO_4: {}", "bar");
			logger.info("FOO_5: {}", (Supplier<String>) () -> "bar");
		}
	}
}