package tk.labyrinth.misc4j2.exception.wrapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExceptionWrapperTest {

	@Test
	void testUnwrap() {
		RuntimeException exception = new RuntimeException();
		//
		Assertions.assertSame(exception,
				new WrappingRuntimeException(exception).unwrap());
		Assertions.assertSame(exception,
				new WrappingError(
						new WrappingRuntimeException(exception)).unwrap());
		Assertions.assertSame(exception,
				new WrappingError(
						new WrappingRuntimeException(
								new WrappingRuntimeException(exception))).unwrap());
	}
}
