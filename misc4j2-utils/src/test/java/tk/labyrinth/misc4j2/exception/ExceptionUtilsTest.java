package tk.labyrinth.misc4j2.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.exception.wrapping.WrappingError;
import tk.labyrinth.misc4j2.exception.wrapping.WrappingRuntimeException;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Arrays;

class ExceptionUtilsTest {

	@Test
	void testRender() {
		Assertions.assertEquals(
				"type = java.math.BigDecimal & value = 12.0",
				ExceptionUtils.render(new BigDecimal("12.0")));
		Assertions.assertEquals(
				"type = java.lang.Double & value = 12.0",
				ExceptionUtils.render(Double.valueOf("12")));
		//
		Assertions.assertEquals(
				"null",
				ExceptionUtils.render(null));
	}

	@Test
	void testRenderList() {
		Assertions.assertEquals(
				"[type = java.math.BigDecimal & value = 12.0, type = java.lang.Double & value = 12.0, null]",
				ExceptionUtils.renderList(Arrays.asList(new BigDecimal("12.0"), Double.valueOf("12"), null)));
	}

	@Test
	void testThrowUnchecked() {
		ContribAssertions.assertThrows(
				() -> ExceptionUtils.throwUnchecked(new IOException("or nio?")),
				fault -> {
					Assertions.assertEquals(WrappingRuntimeException.class, fault.getClass());
					Assertions.assertEquals("java.io.IOException: or nio?", fault.getMessage());
					//
					Assertions.assertNotNull(fault.getCause());
					Assertions.assertEquals(IOException.class, fault.getCause().getClass());
					Assertions.assertEquals("or nio?", fault.getCause().getMessage());
				});
		ContribAssertions.assertThrows(
				() -> ExceptionUtils.throwUnchecked(new SQLException("ACID")),
				fault -> {
					Assertions.assertEquals(WrappingRuntimeException.class, fault.getClass());
					Assertions.assertEquals("java.sql.SQLException: ACID", fault.getMessage());
					//
					Assertions.assertNotNull(fault.getCause());
					Assertions.assertEquals(SQLException.class, fault.getCause().getClass());
					Assertions.assertEquals("ACID", fault.getCause().getMessage());
				});
		ContribAssertions.assertThrows(
				() -> ExceptionUtils.throwUnchecked(new Throwable()),
				fault -> {
					Assertions.assertEquals(WrappingError.class, fault.getClass());
					Assertions.assertEquals("java.lang.Throwable", fault.getMessage());
					//
					Assertions.assertNotNull(fault.getCause());
					Assertions.assertEquals(Throwable.class, fault.getCause().getClass());
					Assertions.assertNull(fault.getCause().getMessage());
				});
	}
}
