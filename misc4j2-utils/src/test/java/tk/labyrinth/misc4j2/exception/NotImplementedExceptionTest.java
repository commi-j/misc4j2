package tk.labyrinth.misc4j2.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

class NotImplementedExceptionTest {

	private void notImplementedMethod() {
		throw new NotImplementedException();
	}

	@Test
	void testNoArgsConstructor() {
		ContribAssertions.assertThrows(this::notImplementedMethod, throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j2.exception.NotImplementedExceptionTest.notImplementedMethod(NotImplementedExceptionTest.java:10)",
					throwable.getMessage());
		});
		ContribAssertions.assertThrows(() -> {
			throw new NotImplementedException();
		}, throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j2.exception.NotImplementedExceptionTest.lambda$testNoArgsConstructor$1(NotImplementedExceptionTest.java:21)",
					throwable.getMessage());
		});
		ContribAssertions.assertThrows(() -> {
			//noinspection TrivialFunctionalExpressionUsage
			((Runnable) () -> {
				throw new NotImplementedException();
			}).run();
		}, throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j2.exception.NotImplementedExceptionTest.lambda$testNoArgsConstructor$3(NotImplementedExceptionTest.java:30)",
					throwable.getMessage());
		});
	}
}
