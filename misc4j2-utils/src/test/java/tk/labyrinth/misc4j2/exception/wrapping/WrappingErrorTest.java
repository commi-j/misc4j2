package tk.labyrinth.misc4j2.exception.wrapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

class WrappingErrorTest {

	@Test
	void testConstructor() {
		RuntimeException exception = new RuntimeException();
		{
			WrappingError wrapper = new WrappingError(exception);
			Assertions.assertSame(exception, wrapper.getCause());
		}
		{
			//noinspection ConstantConditions, ThrowableNotThrown
			ContribAssertions.assertThrows(() -> new WrappingError(null), fault -> {
				Assertions.assertEquals(NullPointerException.class, fault.getClass());
				Assertions.assertEquals("cause", fault.getMessage());
			});
		}
	}
}
