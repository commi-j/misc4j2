package tk.labyrinth.misc4j2.exception.wrapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

class WrappingRuntimeExceptionTest {

	@Test
	void testConstructor() {
		RuntimeException exception = new RuntimeException();
		{
			WrappingRuntimeException wrapper = new WrappingRuntimeException(exception);
			Assertions.assertSame(exception, wrapper.getCause());
		}
		{
			//noinspection ConstantConditions, ThrowableNotThrown
			ContribAssertions.assertThrows(() -> new WrappingRuntimeException(null), fault -> {
				Assertions.assertEquals(NullPointerException.class, fault.getClass());
				Assertions.assertEquals("cause", fault.getMessage());
			});
		}
	}
}
