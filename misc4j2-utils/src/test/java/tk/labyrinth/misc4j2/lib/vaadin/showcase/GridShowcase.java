package tk.labyrinth.misc4j2.lib.vaadin.showcase;

import com.vaadin.flow.component.grid.AbstractGridMultiSelectionModel;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.dom.Element;
import org.junit.jupiter.api.Test;

/**
 * FIXME: This class is somewhy not detected by surefire.
 *
 * @see Grid
 * @see TreeGrid
 */
public class GridShowcase {

	/**
	 * {@link Grid#setSelectionMode(Grid.SelectionMode) Grid.setSelectionMode}({@link Grid.SelectionMode#MULTI}) inserts Selection Column
	 * into the beginning of the Grid. This Column is not accessible via Grid API (like {@link Grid#getColumns()}),
	 * but you may access it through {@link Grid#getElement()}.
	 *
	 * @see AbstractGridMultiSelectionModel#AbstractGridMultiSelectionModel(Grid)
	 */
	@Test
	void accessSelectionColumn() {
		Grid<?> grid = new Grid<>();
		{
			// This action should be done before following manupulations.
			// See how Selection Column is added in constructor of AbstractGridMultiSelectionModel.
			grid.setSelectionMode(Grid.SelectionMode.MULTI);
			grid.getElement().getNode().runWhenAttached(ui -> {
				Element selectionColumnElement = grid.getElement().getChild(0);
				//
				{
					// Removal of Selection Column is one of the options, but if you want to relocate it
					// use one of the methods below.
					grid.getElement().removeChild(selectionColumnElement);
				}
				{
					// Adding to the last position.
					grid.getElement().appendChild(selectionColumnElement);
				}
				{
					// Inserting into specified position.
					grid.getElement().insertChild(1, selectionColumnElement);
				}
			});
		}
	}
}
