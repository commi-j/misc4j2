package tk.labyrinth.misc4j2.lib.vavr;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class VavrUtilsTest {

	@Test
	void testFlattenRecursively() {
		Assertions
				.assertThat(VavrUtils.flattenRecursively(
						List.of("abcdefg", "xyz"),
						element -> element.length() > 1
								? List.ofAll(element.toCharArray())
								.grouped((int) Math.ceil(element.length() / 2d))
								.map(List::mkString)
								.toList()
								: List.empty()))
				.isEqualTo(List.of(
						"abcdefg", "abcd", "ab", "a", "b", "cd", "c", "d", "efg", "ef", "e", "f", "g",
						"xyz", "xy", "x", "y", "z"));
	}
}