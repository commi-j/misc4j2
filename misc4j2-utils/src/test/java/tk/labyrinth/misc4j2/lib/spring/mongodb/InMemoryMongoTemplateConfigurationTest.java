package tk.labyrinth.misc4j2.lib.spring.mongodb;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootTest(classes = InMemoryMongoTemplateConfiguration.class)
class InMemoryMongoTemplateConfigurationTest {

	@Test
	void qwe() {
		System.out.println();
	}

	@Test
	void testInMemoryMongoTemplate(@Autowired MongoTemplate mongoTemplate) {
		DBObject objectToSave = BasicDBObjectBuilder.start().add("key", "value").get();
		//
		mongoTemplate.save(objectToSave, "collection");
		//
		Assertions.assertEquals("value", mongoTemplate.findAll(
				DBObject.class, "collection").get(0).get("key"));
	}
}
