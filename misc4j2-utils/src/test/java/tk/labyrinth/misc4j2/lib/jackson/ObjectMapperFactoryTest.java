package tk.labyrinth.misc4j2.lib.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;
import java.util.Objects;

class ObjectMapperFactoryTest {

	@Test
	void testNullNotSerialized() {
		// TODO
	}

	@Test
	void testPrettyPrinterUsesConsistentLinefeed() throws IOException {
		// TODO: Find a way to test with different line.separator,
		//  but we need it to be set before DefaultIndenter class is loaded in this JVM.
		//  Probably load another instance of it specifically for this test.
		// TODO: Probably we may just configure different linefeed and see that change actually works.
		// System.setProperty("line.separator", "^_^");
		//
		ObjectMapper objectMapper = ObjectMapperFactory.defaultConfigured();
		//
		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.set("foo", new TextNode("bar"));
		String objectNodeContent = objectNode.toPrettyString();
		//
		Assertions.assertEquals(
				"""
						{
						  "foo" : "bar"
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectNode));
	}

	@Test
	void testUsingWriterCreatedBeforeFullConfiguration() throws JsonProcessingException {
		Instant now = Instant.now();
		String formattedString = """
				%s.%s""".formatted(now.getEpochSecond(), now.getNano());
		String rawString = """
				{"nano":%s,"epochSecond":%s}""".formatted(now.getNano(), now.getEpochSecond());
		//
		ObjectMapper objectMapper = new ObjectMapper();
		//
//		// Ensuring it does not write proper value by default.
//		Assertions.assertEquals(rawString, objectMapper.writeValueAsString(dummyWithInstant));
		//
		ObjectWriter objectWriter = objectMapper.writerFor(Instant.class);
		//
		objectMapper.registerModule(new JavaTimeModule());
		//
		Assertions.assertEquals(formattedString, objectMapper.writeValueAsString(now));
		Assertions.assertEquals(rawString, objectWriter.writeValueAsString(now));
	}

	@Test
	void testVisibilityIgnoringMethods() throws JsonProcessingException {
		ObjectMapper plainObjectMapper = new ObjectMapper();
		ObjectMapper configuredObjectMapper = ObjectMapperFactory.defaultConfigured();
		{
			Assertions.assertEquals("{\"value\":24,\"one\":1}", plainObjectMapper.writeValueAsString(new Dummy()));
			Assertions.assertEquals("{\"value\":12}", configuredObjectMapper.writeValueAsString(new Dummy()));
		}
		{
			Assertions.assertEquals(new Dummy(24), plainObjectMapper.readValue("{\"value\":12}", Dummy.class));
			Assertions.assertEquals(new Dummy(12), configuredObjectMapper.readValue("{\"value\":12}", Dummy.class));
		}
	}

	private static class Dummy {

		private int value = 12;

		public Dummy() {
			// no-op
		}

		public Dummy(int value) {
			this.value = value;
		}

		@Override
		@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
		public boolean equals(Object o) {
			return value == ((Dummy) o).value;
		}

		public int getOne() {
			return 1;
		}

		public int getValue() {
			return value * 2;
		}

		@Override
		public int hashCode() {
			return Objects.hash(value);
		}

		public void setValue(int value) {
			this.value = value * 2;
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	private static class DummyWithInstant {

		Instant instant;
	}
}

