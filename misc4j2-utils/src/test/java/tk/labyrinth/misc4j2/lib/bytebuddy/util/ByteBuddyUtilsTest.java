package tk.labyrinth.misc4j2.lib.bytebuddy.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.java.lang.reflect.ParameterUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ByteBuddyUtilsTest {

	@Test
	void testGenerateParameterizedChildWithClasses() {
		{
			Class<?> type = ByteBuddyUtils.generateParameterizedChild(ArrayList.class, String.class);
			ContribAssertions.assertStartsWith("net.bytebuddy.renamed.java.util.ArrayListOfStringByByteBuddy$",
					type.getCanonicalName());
			Assertions.assertTrue(List.class.isAssignableFrom(type));
			Assertions.assertEquals(String.class, ParameterUtils.getFirstActualParameter(type, List.class));
		}
		{
			Class<?> type = ByteBuddyUtils.generateParameterizedChild(HashMap.class, String.class, Integer.class);
			ContribAssertions.assertStartsWith("net.bytebuddy.renamed.java.util.HashMapOfStringIntegerByByteBuddy$",
					type.getCanonicalName());
			Assertions.assertTrue(Map.class.isAssignableFrom(type));
			Assertions.assertEquals(String.class, ParameterUtils.getFirstActualParameter(type, Map.class));
			Assertions.assertEquals(Integer.class, ParameterUtils.getSecondActualParameter(type, Map.class));
		}
	}
}
