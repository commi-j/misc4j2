package tk.labyrinth.misc4j2.lib.commons.lang3.reflect;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import tk.labyrinth.misc4j2.java.lang.reflect.ParameterUtils;

import java.util.Collection;
import java.util.List;

class TypeUtilsTest {

	@Test
	void testIsAssignable() {
		// This is a bug of Apache commons lib, because this combination is not assignable from the JLS point of view:
		// MySpecificInterface has parameter that extends Collection and
		// MySpecificWrapper expects parameter that itself has parameter extending at least List.
		Assertions.assertThrows(
				AssertionFailedError.class,
				() -> Assertions.assertFalse(TypeUtils.isAssignable(
						MySpecificInterface.class,
						ParameterUtils.getFirstActualParameter(MySpecificWrapper.class, MyWrapperBase.class))));
	}

	private interface MyInterfaceBase<T> {
		// empty
	}

	private interface MySpecificInterface<T extends Collection<T>> extends MyInterfaceBase<T> {
		// empty
	}

	private interface MySpecificWrapper extends MyWrapperBase<MyInterfaceBase<? extends List<?>>> {
		// empty
	}

	private interface MyWrapperBase<T> {
		// empty
	}
}