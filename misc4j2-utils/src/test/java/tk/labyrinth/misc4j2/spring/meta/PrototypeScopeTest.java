package tk.labyrinth.misc4j2.spring.meta;

import org.springframework.context.annotation.Scope;
import tk.labyrinth.misc4j2.lib.spring.meta.PrototypeScope;
import tk.labyrinth.misc4j2.test.meta.MetaAnnotationTestBase;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.util.stream.Stream;

public class PrototypeScopeTest extends MetaAnnotationTestBase<PrototypeScope> {

	@Override
	protected Stream<Class<? extends Annotation>> getAnnotationTypes() {
		return Stream.of(Scope.class);
	}

	@Override
	protected Stream<ElementType> getElementTypes() {
		return Stream.of(ElementType.METHOD, ElementType.TYPE);
	}

	@Override
	protected Class<PrototypeScope> getType() {
		return PrototypeScope.class;
	}
}
