package tk.labyrinth.misc4j2.spring.meta;

import org.springframework.stereotype.Component;
import tk.labyrinth.misc4j2.lib.spring.meta.PrototypeScope;
import tk.labyrinth.misc4j2.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.misc4j2.test.meta.MetaAnnotationTestBase;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.util.stream.Stream;

public class PrototypeScopedComponentTest extends MetaAnnotationTestBase<PrototypeScopedComponent> {

	@Override
	protected Stream<Class<? extends Annotation>> getAnnotationTypes() {
		return Stream.of(Component.class, PrototypeScope.class);
	}

	@Override
	protected Stream<ElementType> getElementTypes() {
		return Stream.of(ElementType.TYPE);
	}

	@Override
	protected Class<PrototypeScopedComponent> getType() {
		return PrototypeScopedComponent.class;
	}
}
