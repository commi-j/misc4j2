package tk.labyrinth.misc4j2.java.util;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class PropertiesBuilder {

	Map<String, String> properties = new HashMap<>();

	public Properties build() {
		Properties result = new Properties();
		result.putAll(properties);
		return result;
	}

	public PropertiesBuilder with(String name, String value) {
		Objects.requireNonNull(name, "name");
		Objects.requireNonNull(value, "value");
		//
		return withNullable(name, value);
	}

	public PropertiesBuilder withAll(Properties properties) {
		Objects.requireNonNull(properties, "properties");
		//
		properties.stringPropertyNames().forEach(propertyName ->
				this.properties.put(propertyName, properties.getProperty(propertyName)));
		return this;
	}

	public PropertiesBuilder withNullable(String name, @Nullable String value) {
		if (value != null) {
			properties.put(name, value);
		} else {
			properties.remove(name);
		}
		return this;
	}

	public PropertiesBuilder without(String name) {
		Objects.requireNonNull(name, "name");
		//
		return withNullable(name, null);
	}

	public static PropertiesBuilder empty() {
		return new PropertiesBuilder();
	}

	public static PropertiesBuilder from(Properties properties) {
		return new PropertiesBuilder().withAll(properties);
	}
}
