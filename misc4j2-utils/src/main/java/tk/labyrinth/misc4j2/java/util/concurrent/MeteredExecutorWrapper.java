package tk.labyrinth.misc4j2.java.util.concurrent;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;

/**
 * {@link Executor} wrapper that reports {@link Gauge} metric for each {@link Thread} executing commands.<br>
 * It reports time of execution start so that you can compute duration of current execution.<br>
 * When {@link Thread} is idle, reported value is -1.
 *
 * @author Commitman
 * @version 1.0.0
 */
@RequiredArgsConstructor
public class MeteredExecutorWrapper implements Executor {

	public static final String JAVA_THREAD_EXECUTION_STARTED_AT_METRIC_NAME = "java_thread_execution_started_at";

	public static final String METER_NAME_TAG_NAME = "meterName";

	public static final String THREAD_NAME_TAG_NAME = "threadName";

	private final ThreadLocal<AtomicLong> executionStartedAtAtomicThreadLocal = new ThreadLocal<>();

	private final Executor executor;

	private final String meterName;

	private AtomicLong getExecutionStartedAtAtomic() {
		AtomicLong result;
		{
			AtomicLong existingExecutionStartedAtAtomic = executionStartedAtAtomicThreadLocal.get();
			if (existingExecutionStartedAtAtomic != null) {
				result = existingExecutionStartedAtAtomic;
			} else {
				// We use AtomicLong as a gauge argument because Micrometer accesses it from multiple threads.
				AtomicLong newExecutionStartedAtAtomic = new AtomicLong(-1);
				//
				Metrics.gauge(
						JAVA_THREAD_EXECUTION_STARTED_AT_METRIC_NAME,
						List.of(
								Tag.of(METER_NAME_TAG_NAME, meterName),
								Tag.of(THREAD_NAME_TAG_NAME, Thread.currentThread().getName())),
						newExecutionStartedAtAtomic);
				//
				executionStartedAtAtomicThreadLocal.set(newExecutionStartedAtAtomic);
				//
				result = newExecutionStartedAtAtomic;
			}
		}
		return result;
	}

	@Override
	public void execute(@NonNull Runnable command) {
		executor.execute(() -> {
			AtomicLong executionStartedAtAtomic = getExecutionStartedAtAtomic();
			//
			executionStartedAtAtomic.set(System.currentTimeMillis());
			try {
				command.run();
			} finally {
				executionStartedAtAtomic.set(-1);
			}
		});
	}
}
