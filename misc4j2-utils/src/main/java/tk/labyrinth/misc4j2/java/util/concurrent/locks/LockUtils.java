package tk.labyrinth.misc4j2.java.util.concurrent.locks;

import java.util.concurrent.locks.Lock;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class LockUtils {

	/**
	 * @param lock     non-null
	 * @param runnable non-null
	 *
	 * @since 1.0.0
	 */
	public static void locked(Lock lock, Runnable runnable) {
		lock.lock();
		//
		try {
			runnable.run();
		} finally {
			lock.unlock();
		}
	}
}
