package tk.labyrinth.misc4j2.java.math;

import java.math.BigDecimal;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class BigDecimalUtils {

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is greater than <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean gt(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) > 0;
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is greater than or equal to <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean gte(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) >= 0;
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is less than <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean lt(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) < 0;
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is less than or equal to <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean lte(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) <= 0;
	}
}
