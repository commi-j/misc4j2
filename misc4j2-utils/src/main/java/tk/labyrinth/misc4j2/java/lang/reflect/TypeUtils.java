package tk.labyrinth.misc4j2.java.lang.reflect;

import io.vavr.collection.List;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.9
 */
public class TypeUtils {

	/**
	 * @param javaType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.9
	 */
	public static Class<?> asClass(Type javaType) {
		return (Class<?>) javaType;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static ParameterizedType asParameterizedType(Type javaType) {
		return (ParameterizedType) javaType;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.9
	 */
	public static WildcardType asWildcardType(Type javaType) {
		return (WildcardType) javaType;
	}

	/**
	 * @param type non-null
	 *
	 * @return nullable
	 *
	 * @see #findClassInferred(Type)
	 * @see #getClass(Type)
	 * @since 1.0.2
	 */
	@Nullable
	public static Class<?> findClass(Type type) {
		Class<?> result;
		if (type instanceof Class) {
			result = (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			result = (Class<?>) ((ParameterizedType) type).getRawType();
		} else if (type instanceof TypeVariable<?>) {
			result = null;
		} else if (type instanceof WildcardType) {
			result = null;
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	/**
	 * @param type    non-null
	 * @param <T>Type
	 *
	 * @return nullable
	 *
	 * @see #findClass(Type)
	 * @see #getClassInferred(Type)
	 * @since 1.0.4
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> Class<T> findClassInferred(Type type) {
		return (Class<T>) findClass(type);
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 *
	 * @see #findClass(Type)
	 * @see #getClassInferred(Type)
	 * @since 1.0.2
	 */
	public static Class<?> getClass(Type type) {
		Class<?> result = findClass(type);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: type = " + type);
			}
		}
		return result;
	}

	/**
	 * @param type    non-null
	 * @param <T>Type
	 *
	 * @return non-null
	 *
	 * @see #findClassInferred(Type)
	 * @see #getClass(Type)
	 * @since 1.0.4
	 */
	public static <T> Class<T> getClassInferred(Type type) {
		Class<T> result = findClassInferred(type);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: type = " + type);
			}
		}
		return result;
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getSimpleName(Type type) {
		String result;
		if (type instanceof Class) {
			result = ((Class<?>) type).getSimpleName();
		} else {
			result = type.toString();
		}
		return result;
	}

	/**
	 * Some implementations treat ? as having upper bound as array with 0 length.
	 * Some as an array with Object class.
	 * This method checks both cases to tell whether this type has a meaningful upper bound.
	 *
	 * @param wildcardType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.9
	 */
	public static boolean hasEffectiveUpperBound(WildcardType wildcardType) {
		return wildcardType.getUpperBounds().length > 1 ||
				(wildcardType.getUpperBounds().length == 1 && wildcardType.getUpperBounds()[0] != Object.class);
	}

	/**
	 * @param childType  non-null
	 * @param parentType non-null
	 *
	 * @return true if assignable to, false otherwise
	 *
	 * @since 1.0.5
	 */
	public static boolean isAssignableTo(Type childType, Type parentType) {
		boolean result;
		{
			Class<?> childClass = findClass(childType);
			Class<?> parentClass = findClass(parentType);
			//
			if (parentClass != null) {
				// Parent is declared -> checking child.
				//
				if (childClass != null) {
					// Child is also declared -> comparing classes and parameters.
					//
					if (parentClass.isAssignableFrom(childClass)) {
						// Parent is assignable from child -> checking parent for generic.
						//
						if (parentClass.getTypeParameters().length > 0) {
							// Parent is generic -> checking for non-raw.
							//
							if (isParameterizedType(parentType)) {
								// Parent is parameterized -> comparing parameters.
								result = IntStream
										.range(0, parentClass.getTypeParameters().length)
										.allMatch(index -> {
											Type childParameter = ParameterUtils.getActualParameter(
													childType,
													parentClass,
													index);
											Type parentParameter = ((ParameterizedType) parentType)
													.getActualTypeArguments()[index];
											return isAssignableTo(childParameter, parentParameter);
										});
							} else {
								// Parent is raw -> looks like there are no raw types in runtime.
								//
								throw new NotImplementedException();
							}
						} else {
							// Parent is plain -> true.
							//
							result = true;
						}
					} else {
						// Class mismatch -> false.
						//
						result = false;
					}
				} else {
					// Child is variable or wildcard -> checking kind.
					//
					if (childType instanceof TypeVariable<?> childVariableType) {
						// Child is variable -> checking bounds.
						//
						if (childVariableType.getBounds().length > 0) {
							// Has bounds -> checking bounds number.
							//
							if (childVariableType.getBounds().length > 1) {
								// Multiple bounds.
								//
								throw new NotImplementedException();
							} else {
								// Single bound -> extracting for comparison.
								//
								result = isAssignableTo(childVariableType.getBounds()[0], parentType);
							}
						} else {
							// No bounds.
							//
							throw new NotImplementedException();
						}
					} else if (childType instanceof WildcardType childWildcardType) {
						// Child is wildcard -> checking bounds.
						//
						Type childBoundType;
						{
							if (childWildcardType.getUpperBounds().length > 0) {
								// ? extends UpperBound -> use it;
								//
								childBoundType = List.of(childWildcardType.getUpperBounds()).single();
							} else if (childWildcardType.getLowerBounds().length > 0) {
								// We don't support super so far.
								//
								throw new NotImplementedException();
							} else {
								// ? -> Object;
								//
								childBoundType = Object.class;
							}
						}
						return isAssignableTo(childBoundType, parentType);
					} else {
						// Unknown.
						//
						throw new NotImplementedException();
					}
				}
			} else {
				// Parent is variable or wildcard -> checking kind.
				//
				if (parentType instanceof TypeVariable<?>) {
					// Parent is variable.
					//
					throw new NotImplementedException();
				} else if (parentType instanceof WildcardType) {
					// Parent is wildcard -> checking bounds.
					//
					WildcardType wildcardParentType = (WildcardType) parentType;
					if (!WildcardTypeUtils.hasLowerBounds(wildcardParentType)) {
						// Has no lower bounds -> comparing bounds.
						//
						result = WildcardTypeUtils
								.getUpperBoundStream(wildcardParentType)
								.allMatch(parentUpperBound -> isAssignableTo(childType, parentUpperBound));
					} else {
						// Has lower bounds.
						//
						throw new NotImplementedException();
					}
				} else {
					// Parent is not variable or wildcard.
					//
					throw new UnsupportedOperationException();
				}
			}
		}
		return result;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return true if this type is class, false otherwise
	 *
	 * @since 1.0.9
	 */
	public static boolean isClass(Type javaType) {
		return javaType instanceof Class<?>;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return true if this type is parameterized, false otherwise
	 *
	 * @since 1.0.3
	 */
	public static boolean isParameterizedType(Type javaType) {
		return javaType instanceof ParameterizedType;
	}

	/**
	 * @param javaType nullable
	 *
	 * @return true if this type is plain, false otherwise
	 *
	 * @since 1.0.5
	 */
	public static boolean isPlainType(@CheckForNull Type javaType) {
		return ClassUtils.isPlain(findClass(javaType));
	}

	/**
	 * @param javaType non-null
	 *
	 * @return true if this type is wildcard, false otherwise
	 *
	 * @since 1.0.9
	 */
	public static boolean isWildcardType(Type javaType) {
		return javaType instanceof WildcardType;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return javaType
	 *
	 * @since 1.0.8
	 */
	public static Type requirePlain(Type javaType) {
		if (!isPlainType(javaType)) {
			throw new IllegalArgumentException("Require plain: %s".formatted(javaType));
		}
		return javaType;
	}

	/**
	 * @param javaType non-null
	 *
	 * @return javaType
	 *
	 * @since 1.0.6
	 */
	public static Type requireResolved(Type javaType) {
		if (!isPlainType(javaType)) {
			if (javaType instanceof Class) {
				// no-op
			} else if (isParameterizedType(javaType)) {
				try {
					Stream.of(asParameterizedType(javaType).getActualTypeArguments())
							.forEach(TypeUtils::requireResolved);
				} catch (IllegalArgumentException ex) {
					throw new IllegalArgumentException("Require resolved: %s".formatted(javaType));
				}
			} else {
				throw new IllegalArgumentException("Require resolved: %s".formatted(javaType));
			}
		}
		return javaType;
	}
}
