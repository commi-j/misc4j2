package tk.labyrinth.misc4j2.java.lang.reflect;

import lombok.Getter;
import lombok.Setter;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;

/**
 * TODO: Add exception handling policy description.
 *
 * @author Commitman
 * @version 1.0.3
 */
public class DynamicProxyHandler<T> implements InvocationHandler {

	@Getter
	private final Class<T> type;

	@Getter
	@SuppressWarnings("NotNullFieldNotInitialized")
	private T proxy;

	@Getter
	@Nullable
	@Setter
	private T target;

	private DynamicProxyHandler(Class<T> type) {
		this.type = Objects.requireNonNull(type, "type");
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) {
		if (target == null) {
			throw new IllegalStateException("target is null");
		}
		//
		try {
			Object result;
			if (Objects.equals(method.getName(), "equals") && method.getParameterCount() == 1) {
				if (args[0] == proxy) {
					result = true;
				} else {
					result = method.invoke(target, args);
				}
			} else if (Objects.equals(method.getName(), "toString") && method.getParameterCount() == 0) {
				result = "ProxyOf(" + method.invoke(target, args) + ")";
			} else {
				result = method.invoke(target, args);
			}
			return result;
		} catch (InvocationTargetException ex) {
			// TODO: May be use a specific exception type?
			Throwable cause = ex.getCause();
			if (cause instanceof RuntimeException) {
				throw (RuntimeException) cause;
			} else {
				ExceptionUtils.throwUnchecked(cause);
			}
		} catch (Throwable t) {
			ExceptionUtils.throwUnchecked(t);
		}
		throw new UnreachableStateException();
	}

	/**
	 * @param proxy non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	@SuppressWarnings("unchecked")
	public static <T> DynamicProxyHandler<T> ofProxy(T proxy) {
		InvocationHandler invocationHandler = Proxy.getInvocationHandler(proxy);
		return (DynamicProxyHandler<T>) invocationHandler;
	}

	/**
	 * @param type non-null
	 * @param <T>  Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	@SuppressWarnings("unchecked")
	public static <T> DynamicProxyHandler<T> ofType(Class<T> type) {
		DynamicProxyHandler<T> result = new DynamicProxyHandler<>(type);
		result.proxy = (T) Proxy.newProxyInstance(
				Thread.currentThread().getContextClassLoader(),
				new Class[]{type, ProxyMarker.class},
				result);
		return result;
	}
}
