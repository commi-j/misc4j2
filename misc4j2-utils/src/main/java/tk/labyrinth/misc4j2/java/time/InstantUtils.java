package tk.labyrinth.misc4j2.java.time;

import java.time.Instant;
import java.time.ZoneId;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class InstantUtils {

	/**
	 * @param instant non-null
	 * @param months  positive or negative
	 * @param zoneId  non-null
	 *
	 * @return non-null
	 */
	public static Instant plusMonths(Instant instant, int months, ZoneId zoneId) {
		return instant.atZone(zoneId).plusMonths(months).toInstant();
	}
}
