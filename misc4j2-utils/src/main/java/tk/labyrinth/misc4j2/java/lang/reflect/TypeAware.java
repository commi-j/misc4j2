package tk.labyrinth.misc4j2.java.lang.reflect;

import java.lang.reflect.Type;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.1
 */
public interface TypeAware<T> {

	default T createNewInstance() {
		return ReflectionUtils.createNewInstance(getParameterClass());
	}

	@SuppressWarnings("unchecked")
	default Class<T> getParameterClass() {
		return getParameterClass((Class<? extends TypeAware<T>>) getClass());
	}

	@SuppressWarnings("unchecked")
	default Type getParameterType() {
		return getParameterType((Class<? extends TypeAware<?>>) getClass());
	}

	@SuppressWarnings("unchecked")
	static <T> Class<T> getParameterClass(Class<? extends TypeAware<T>> type) {
		return (Class<T>) getRawParameterClass(type);
	}

	static Type getParameterType(Class<? extends TypeAware<?>> type) {
		return getRawParameterType(type);
	}

	static Class<?> getRawParameterClass(Class<? extends TypeAware> type) {
		return TypeUtils.getClass(getRawParameterType(type));
	}

	// Rawtyping is required for 'type' parameter to avoid unnecessary generic casts when invoked.
	static Type getRawParameterType(Class<? extends TypeAware> type) {
		return ParameterUtils.getFirstActualParameter(type, TypeAware.class);
	}
}
