package tk.labyrinth.misc4j2.java.util.regex;

import java.util.regex.Matcher;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class MatcherUtils {

	/**
	 * @param matcher    non-null
	 * @param groupNames non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Stream<String> getGroups(Matcher matcher, Stream<String> groupNames) {
		return groupNames.map(matcher::group);
	}

	/**
	 * @param matcher    non-null
	 * @param groupNames non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Stream<String> getGroups(Matcher matcher, String... groupNames) {
		return getGroups(matcher, Stream.of(groupNames));
	}
}
