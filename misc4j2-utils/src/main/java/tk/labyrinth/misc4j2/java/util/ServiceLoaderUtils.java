package tk.labyrinth.misc4j2.java.util;

import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ServiceLoaderUtils {

	/**
	 * @since 1.0.0
	 */
	public static <S> Set<S> load(Class<S> serviceType) {
		return ServiceLoader.load(serviceType, serviceType.getClassLoader()).stream()
				.map(ServiceLoader.Provider::get)
				.collect(Collectors.toSet());
	}
}
