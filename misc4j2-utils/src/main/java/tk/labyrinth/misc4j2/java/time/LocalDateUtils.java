package tk.labyrinth.misc4j2.java.time;

import java.time.LocalDate;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class LocalDateUtils {

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is greater than <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean gt(LocalDate first, LocalDate second) {
		return first.isAfter(second);
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is greater than or equal to <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean gte(LocalDate first, LocalDate second) {
		return !first.isBefore(second);
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is less than <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean lt(LocalDate first, LocalDate second) {
		return first.isBefore(second);
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return whether <b>first</b> is less than or equal to <b>second</b>
	 *
	 * @since 1.0.0
	 */
	public static boolean lte(LocalDate first, LocalDate second) {
		return !first.isAfter(second);
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return maximum value of two
	 *
	 * @since 1.0.1
	 */
	public static LocalDate max(LocalDate first, LocalDate second) {
		return gte(first, second) ? first : second;
	}

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return minimum value of two
	 *
	 * @since 1.0.1
	 */
	public static LocalDate min(LocalDate first, LocalDate second) {
		return lte(first, second) ? first : second;
	}
}
