package tk.labyrinth.misc4j2.java.lang;

import javax.annotation.CheckForNull;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class IntegerUtils {

	/**
	 * @param first  nullable
	 * @param second nullable
	 *
	 * @return -1, 0 or 1
	 *
	 * @since 1.0.0
	 */
	public static int compareNullsLast(@CheckForNull Integer first, @CheckForNull Integer second) {
		return first != null
				? second != null // First is not null, second...
				? Integer.compare(first, second) // is not null;
				: -1 // is null;
				: second != null // First is null, second...
				? 1 // is not null;
				: 0; // is null;
	}
}
