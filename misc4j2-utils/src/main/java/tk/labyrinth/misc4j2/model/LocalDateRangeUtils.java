package tk.labyrinth.misc4j2.model;

import java.time.LocalDate;
import java.time.Month;

/**
 * @author Commitman
 * @version 1.0.4
 */
public class LocalDateRangeUtils {

	/**
	 * @param year  non-null
	 * @param month non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static LocalDateRange ofMonth(int year, Month month) {
		LocalDate localDate = LocalDate.of(year, month, 1);
		return LocalDateRange.ofFinite(localDate, localDate.plusMonths(1).minusDays(1));
	}
}
