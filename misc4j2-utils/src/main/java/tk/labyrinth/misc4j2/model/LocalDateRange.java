package tk.labyrinth.misc4j2.model;

import lombok.EqualsAndHashCode;

import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 * Represents range between two dates.<br>
 * Any of dates may be null, which represents infinite value.<br>
 *
 * @author Commitman
 * @version 1.0.5
 */
@EqualsAndHashCode
public class LocalDateRange {

	/**
	 * <b>null</b> means infinity.
	 */
	@Nullable
	private final LocalDate from;

	/**
	 * <b>null</b> means infinity.
	 */
	@Nullable
	private final LocalDate to;

	private LocalDateRange(@Nullable LocalDate from, @Nullable LocalDate to) {
		this.from = from;
		this.to = to;
		if (from != null && to != null && from.isAfter(to)) {
			throw new IllegalArgumentException("From is before to: from = " + from + ", to = " + to);
		}
	}

	/**
	 * @param localDate non-null
	 *
	 * @return whether this range contains <b>localDate</b>
	 *
	 * @since 1.0.5
	 */
	public boolean contains(LocalDate localDate) {
		Objects.requireNonNull(localDate, "localDate");
		//
		return (from == null || !localDate.isBefore(from)) &&
				(to == null || !localDate.isAfter(to));
	}

	public LocalDate getFiniteFrom() {
		return Objects.requireNonNull(from, "from");
	}

	/**
	 * Returns inclusive {@link Period} between <b>from</b> and <b>to</b> if this range is finite or fails otherwise.
	 *
	 * @return period of at least one day if finite
	 *
	 * @throws IllegalArgumentException if not finite
	 */
	public Period getFinitePeriod() {
		requireFinite();
		//noinspection ConstantConditions
		return getPeriod();
	}

	public LocalDate getFiniteTo() {
		return Objects.requireNonNull(to, "to");
	}

	@Nullable
	public LocalDate getFrom() {
		return from;
	}

	/**
	 * Returns inclusive {@link Period} between <b>from</b> and <b>to</b> if this range is finite or null otherwise.
	 *
	 * @return period of at least one day if finite, null otherwise
	 */
	@Nullable
	public Period getPeriod() {
		Period result;
		if (isFinite()) {
			//noinspection ConstantConditions
			result = Period.between(from, to).plusDays(1);
		} else {
			result = null;
		}
		return result;
	}

	@Nullable
	public LocalDate getTo() {
		return to;
	}

	public boolean hasFiniteFrom() {
		return from != null;
	}

	public boolean hasFiniteTo() {
		return to != null;
	}

	public boolean isFinite() {
		return hasFiniteFrom() && hasFiniteTo();
	}

	public LocalDateRange requireFinite() {
		if (!isFinite()) {
			throw new IllegalArgumentException("Require finite: " + this);
		}
		return this;
	}

	public LocalDateRange requireFiniteFrom() {
		if (!hasFiniteFrom()) {
			throw new IllegalArgumentException("Require finite from: " + this);
		}
		return this;
	}

	public LocalDateRange requireFiniteTo() {
		if (!hasFiniteTo()) {
			throw new IllegalArgumentException("Require finite to: " + this);
		}
		return this;
	}

	@Override
	public String toString() {
		return "LocalDateRange{" +
				"from=" + (from != null ? from : "inf") +
				", to=" + (to != null ? to : "inf") +
				'}';
	}

	public LocalDateRange withFiniteFrom(LocalDate from) {
		return ofAny(from, to).requireFiniteFrom();
	}

	public LocalDateRange withFiniteTo(LocalDate to) {
		return ofAny(from, to).requireFiniteTo();
	}

	public LocalDateRange withInfiniteFrom() {
		return ofAny(null, to);
	}

	public LocalDateRange withInfiniteTo() {
		return ofAny(from, null);
	}

	public static LocalDateRange ofAny(@Nullable LocalDate from, @Nullable LocalDate to) {
		return new LocalDateRange(from, to);
	}

	public static LocalDateRange ofFinite(LocalDate from, LocalDate to) {
		return ofAny(from, to).requireFinite();
	}

	public static LocalDateRange ofInfiniteFrom(LocalDate to) {
		return ofAny(null, to).requireFiniteTo();
	}

	public static LocalDateRange ofInfiniteTo(LocalDate from) {
		return ofAny(from, null).requireFiniteFrom();
	}

	public static LocalDateRange ofInfinity() {
		return ofAny(null, null);
	}
}
