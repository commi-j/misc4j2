package tk.labyrinth.misc4j2.ioc.context;

import java.lang.reflect.Type;

public interface BeanContext {

	default <T> T get(Class<T> type) {
		return get((Type) type);
	}

	<T> T get(Type type);
}
