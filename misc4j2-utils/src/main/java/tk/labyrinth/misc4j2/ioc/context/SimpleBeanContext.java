package tk.labyrinth.misc4j2.ioc.context;

import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.java.lang.reflect.ReflectionUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleBeanContext implements BeanContext {

	private final Set<Object> instances = new HashSet<>();
//	private final BeanContext parent;

	private final Map<Type, Object> typeInstanceCache = new HashMap<>();

	private final Map<Type, Set<Type>> typeSuppressions = new HashMap<>();

	private final Set<Class<?>> types = new HashSet<>();

	@SuppressWarnings("unchecked")
	private <T> T create(Class<T> type) {
		Set<Class<?>> matchingTypes = types.stream()
				.filter(type::isAssignableFrom)
				.collect(Collectors.toSet());
		if (matchingTypes.size() != 1) {
			throw new IllegalArgumentException("Nope: " + matchingTypes);
		} else {
			Class<?> typeToCreate = matchingTypes.iterator().next();
			List<Constructor<?>> constructors = List.of(typeToCreate.getConstructors());
			if (constructors.size() != 1) {
				throw new IllegalArgumentException("Nope: " + constructors + ", typeToCreate = " + typeToCreate);
			}
			Constructor<?> constructor = constructors.get(0);
			List<Object> arguments = Stream.of(constructor.getParameterTypes())
					.map(this::get)
					.collect(Collectors.toList());
			T result = (T) ReflectionUtils.createNewInstance(constructor, arguments.stream());
			instances.add(result);
			return result;
		}
	}

	private <T> T findOrCreate(Class<T> type) {
		List<T> matchingInstances = instances.stream()
				.filter(type::isInstance)
				.map(type::cast)
				.collect(Collectors.toList());
		if (matchingInstances.size() > 1) {
			throw new IllegalStateException("This must be resolved on type level");
		}
		//
		T result;
		if (!matchingInstances.isEmpty()) {
			result = matchingInstances.get(0);
		} else {
			result = create(type);
			typeInstanceCache.put(type, result);
		}
		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Type type) {
		T result;
		{
			T bean = (T) typeInstanceCache.get(type);
			if (bean != null) {
				result = bean;
			} else {
				// FIXME: Rework cast into proper type resolution.
				result = findOrCreate((Class<T>) type);
				typeInstanceCache.put(type, result);
			}
		}
		return result;
	}

	public void registerConstructor(Type type, Function<BeanContext, Object> constructor) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	public void registerInstance(Object instance) {
		// FIXME: Check for collisions.
		instances.add(instance);
	}

	public void registerType(Class<?> type) {
		Set<Class<?>> collisions = types.stream()
				.filter(innerType -> innerType.isAssignableFrom(type) || type.isAssignableFrom(innerType))
				.collect(Collectors.toSet());
		if (!collisions.isEmpty()) {
			throw new IllegalArgumentException("Type collision: type = " + type + ", collisions = " + collisions);
		}
		types.add(type);
	}

	@SafeVarargs
	public final <T> void registerTypeWithSuppression(Class<T> type, Class<? super T>... suppressedTypes) {
		registerTypeWithSuppression(type, Stream.of(suppressedTypes));
	}

	public <T> void registerTypeWithSuppression(Class<T> type, Stream<Class<? super T>> suppressedTypes) {
		Set<Class<?>> collisions = types.stream()
				.filter(innerType -> innerType.isAssignableFrom(type) || type.isAssignableFrom(innerType))
				.collect(Collectors.toSet());
		if (!collisions.isEmpty()) {
			throw new IllegalArgumentException("Type collision: type = " + type + ", collisions = " + collisions);
		}
		types.add(type);
	}
}
