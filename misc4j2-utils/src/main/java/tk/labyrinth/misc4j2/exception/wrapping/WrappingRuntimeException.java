package tk.labyrinth.misc4j2.exception.wrapping;

import java.util.Objects;

/**
 * {@link RuntimeException} that wraps an {@link Exception}.<br>
 *
 * @author Commitman
 * @version 1.0.0
 * @see ExceptionWrapper#unwrap()
 * @see Throwable#getCause()
 * @see WrappingError
 */
public class WrappingRuntimeException extends RuntimeException implements ExceptionWrapper {

	public WrappingRuntimeException(Exception cause) {
		super(Objects.requireNonNull(cause, "cause"));
	}
}
