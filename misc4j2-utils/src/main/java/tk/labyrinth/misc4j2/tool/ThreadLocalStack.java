package tk.labyrinth.misc4j2.tool;

import io.vavr.collection.List;

import java.util.function.UnaryOperator;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public class ThreadLocalStack<T> {

	private final boolean initializedByDefault;

	private final ThreadLocal<List<T>> threadLocal;

	public ThreadLocalStack(boolean initializedByDefault) {
		this.initializedByDefault = initializedByDefault;
		//
		if (initializedByDefault) {
			threadLocal = ThreadLocal.withInitial(List::empty);
		} else {
			threadLocal = new ThreadLocal<>();
		}
	}

	public void discard() {
		if (initializedByDefault) {
			throw new IllegalStateException("Calling discard on initializedByDefault");
		}
		//
		List<T> localStack = threadLocal.get();
		//
		if (localStack == null) {
			throw new IllegalStateException("Require initialized");
		}
		//
		threadLocal.set(null);
	}

	public T get() {
		List<T> localStack = requireNonEmpty();
		//
		return localStack.last();
	}

	public List<T> getStack() {
		return requireInitialized();
	}

	public void init() {
		if (initializedByDefault) {
			throw new IllegalStateException("Calling init on initializedByDefault");
		}
		//
		List<T> localStack = threadLocal.get();
		//
		if (localStack != null) {
			throw new IllegalStateException("Require not initialized: %s".formatted(localStack));
		}
		//
		threadLocal.set(List.empty());
	}

	public void pop() {
		List<T> localStack = requireNonEmpty();
		//
		threadLocal.set(localStack.init());
	}

	public void push(T value) {
		requireInitialized();
		//
		threadLocal.set(threadLocal.get().append(value));
	}

	public List<T> requireInitialized() {
		List<T> localStack = threadLocal.get();
		//
		if (localStack == null) {
			throw new IllegalStateException("Require initialized");
		}
		//
		return localStack;
	}

	public List<T> requireNonEmpty() {
		List<T> localStack = requireInitialized();
		//
		if (localStack.isEmpty()) {
			throw new IllegalStateException("Require non-empty: %s".formatted(localStack));
		}
		//
		return localStack;
	}

	public void update(UnaryOperator<T> updateOperator) {
		List<T> localStack = requireNonEmpty();
		//
		List<T> nextLocalStack = localStack.update(localStack.size() - 1, updateOperator);
		//
		threadLocal.set(nextLocalStack);
	}

	public void within(Runnable runnable) {
		if (initializedByDefault) {
			throw new IllegalStateException("Calling within on initializedByDefault");
		}
		//
		init();
		//
		try {
			runnable.run();
		} finally {
			discard();
		}
	}
}
