package tk.labyrinth.misc4j2.common.merge;

/**
 * @author Commitman
 * @version 1.0.0
 */
// TODO: WIP
public class MergeException extends RuntimeException {

	private final Object first;

	private final String property;

	private final Object second;

	public MergeException(Object first, Object second, String property, String message) {
		this.first = first;
		this.property = property;
		this.second = second;
	}
}
