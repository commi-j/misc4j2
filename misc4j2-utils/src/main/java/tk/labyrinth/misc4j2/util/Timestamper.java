package tk.labyrinth.misc4j2.util;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Utility class to obtain unique timestamp values.<br>
 * In most cases it simply returns value of {@link System#currentTimeMillis()},
 * but when invoked multiple times within one millisecond it returns incremented values.
 *
 * @author Commitman
 * @version 1.0.0
 */
// TODO: Add granularity.
public class Timestamper {

	private final AtomicLong cache = new AtomicLong(Long.MIN_VALUE);

	/**
	 * @return {@link System#currentTimeMillis()} or larger
	 */
	public long get() {
		return cache.updateAndGet(lastTimestamp -> {
			long currentTimeMillis = System.currentTimeMillis();
			return currentTimeMillis > lastTimestamp
					? currentTimeMillis
					: lastTimestamp + 1;
		});
	}
}
