package tk.labyrinth.misc4j2.text;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class UnicodeUtils {

	/**
	 * Non-breaking space<br>
	 * <a href="https://en.wikipedia.org/wiki/Non-breaking_space">https://en.wikipedia.org/wiki/Non-breaking_space</a>
	 *
	 * @since 1.0.0
	 */
	public static final String NBSP = " ";
}
