package tk.labyrinth.misc4j2.lib.vaadin.tool.fault;

/**
 * @see VaadinFaultHandlerProcessor
 */
public interface VaadinFaultHandler {

	void handle(Throwable fault);
}
