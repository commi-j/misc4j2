package tk.labyrinth.misc4j2.lib.spring.reflection;

import org.springframework.core.convert.TypeDescriptor;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class TypeDescriptorUtils {

	/**
	 * @param typeDescriptor non-null
	 * @param index          non-negative
	 *
	 * @return non-null
	 */
	public static TypeDescriptor getParameter(TypeDescriptor typeDescriptor, int index) {
		return new TypeDescriptor(typeDescriptor.getResolvableType().getGeneric(index), null, null);
	}
}
