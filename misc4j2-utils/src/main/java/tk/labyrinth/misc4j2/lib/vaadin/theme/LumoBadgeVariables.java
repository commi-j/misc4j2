package tk.labyrinth.misc4j2.lib.vaadin.theme;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class LumoBadgeVariables {

	public static final String BADGE = "badge";

	public static final String CONTRAST = "contrast";

	public static final String ERROR = "error";

	public static final String PILL = "pill";

	public static final String PRIMARY = "primary";

	public static final String SMALL = "small";

	public static final String SUCCESS = "success";

	public static final String WARNING = "warning";
}
