package tk.labyrinth.misc4j2.lib.reactor;

import lombok.NonNull;
import reactor.core.publisher.Flux;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class FluxUtils {

	/**
	 * @param flux non-null
	 * @param <T>  Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static <T> T lookupValue(@NonNull Flux<T> flux) {
		T result;
		//
		try {
			result = flux.blockFirst(Duration.ofNanos(0));
		} catch (IllegalStateException ex) {
			if (Objects.equals(
					ex.toString(),
					"java.lang.IllegalStateException: Timeout on blocking read for 0 NANOSECONDS")) {
				result = null;
			} else {
				throw ex;
			}
		}
		//
		return result;
	}
}
