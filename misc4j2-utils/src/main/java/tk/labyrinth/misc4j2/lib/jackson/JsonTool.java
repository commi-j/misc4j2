package tk.labyrinth.misc4j2.lib.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class JsonTool {

	private final ObjectWriter defaultObjectWriter;

	private final ObjectWriter prettyObjectWriter;

	public JsonTool(ObjectMapper objectMapper) {
		defaultObjectWriter = objectMapper.writer();
		prettyObjectWriter = objectMapper.writerWithDefaultPrettyPrinter();
	}

	/**
	 * @param value non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public String toJson(Object value) {
		try {
			return defaultObjectWriter.writeValueAsString(value);
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param value non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public String toPrettyJson(Object value) {
		try {
			return prettyObjectWriter.writeValueAsString(value);
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}
}
