package tk.labyrinth.misc4j2.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#ROW}).<br>
 * Note: this property may be altered later, even to "column" values.<br>
 */
public class CssHorizontalLayout extends CssFlexboxLayout {

	public CssHorizontalLayout() {
		super(FlexDirection.ROW);
	}

	public CssHorizontalLayout(Component... components) {
		this();
		add(components);
	}
}
