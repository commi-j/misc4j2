package tk.labyrinth.misc4j2.lib.vaadin.tool.setup;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.SessionInitListener;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;

/**
 * {@link SessionInitListener} which is also {@link VaadinServiceInitListener}
 * that listens for {@link ServiceInitEvent} and registers itself with
 * {@link VaadinService#addSessionInitListener}.
 */
public interface AutoregisteringSessionInitListener extends SessionInitListener, VaadinServiceInitListener {

	@Override
	default void serviceInit(ServiceInitEvent event) {
		event.getSource().addSessionInitListener(this);
	}
}
