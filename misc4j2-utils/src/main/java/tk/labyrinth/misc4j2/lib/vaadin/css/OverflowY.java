package tk.labyrinth.misc4j2.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csS/css_overflow.asp">https://www.w3schools.com/csS/css_overflow.asp</a>
 *
 * @author Commitman
 * @version 1.0.0
 * @see Overflow
 * @see OverflowX
 */
@Value(staticConstructor = "of")
public class OverflowY implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY HIDDEN = of("hidden");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY SCROLL = of("scroll");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowY VISIBLE = of("visible");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "overflow-y";
	}
}
