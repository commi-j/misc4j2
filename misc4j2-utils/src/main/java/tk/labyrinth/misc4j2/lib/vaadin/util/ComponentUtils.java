package tk.labyrinth.misc4j2.lib.vaadin.util;

import com.vaadin.flow.component.Component;

import java.util.function.Consumer;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ComponentUtils {

	/**
	 * Accepts component and applies provided {@link Consumer} to it. Returns the component received as first argument.
	 * This method is useful for functional style composition.
	 *
	 * @param component         non-null
	 * @param componentConsumer non-null
	 * @param <C>               Component Type
	 *
	 * @return component
	 *
	 * @since 1.0.0
	 */
	public static <C extends Component> Component process(C component, Consumer<C> componentConsumer) {
		componentConsumer.accept(component);
		//
		return component;
	}
}
