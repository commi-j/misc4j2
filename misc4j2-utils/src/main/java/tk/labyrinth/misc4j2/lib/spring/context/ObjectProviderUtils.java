package tk.labyrinth.misc4j2.lib.spring.context;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.support.ScopeNotActiveException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ObjectProviderUtils {

	/**
	 * @param objectProvider non-null
	 * @param <T>            Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> List<T> tryGet(ObjectProvider<T> objectProvider) {
		return tryGet(
				objectProvider,
				fault -> LoggerFactory.getLogger(ObjectProviderUtils.class).warn(
						"Failed to get object from provider %s".formatted(objectProvider),
						fault));
	}

	/**
	 * @param objectProvider non-null
	 * @param faultConsumer  non-null
	 * @param <T>            Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> List<T> tryGet(
			ObjectProvider<T> objectProvider,
			Consumer<ScopeNotActiveException> faultConsumer) {
		List<T> result;
		{
			Iterator<T> iterator = objectProvider.iterator();
			List<T> objects = new ArrayList<>();
			boolean hasNext = false;
			do {
				boolean reachedNextOrEnd = false;
				while (!reachedNextOrEnd) {
					try {
						hasNext = iterator.hasNext();
						reachedNextOrEnd = true;
					} catch (ScopeNotActiveException ex) {
						faultConsumer.accept(ex);
					}
				}
				if (hasNext) {
					objects.add(iterator.next());
				}
			} while (hasNext);
			result = objects;
		}
		return result;
	}
}
