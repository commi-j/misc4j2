package tk.labyrinth.misc4j2.lib.vaadin.html.table;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

/**
 * @author Commitman
 * @version 1.0.1
 */
@Tag("tr")
public class HtmlTableRow extends HtmlContainer {

	public static HtmlTableRow data(HtmlTableData... datas) {
		HtmlTableRow result = new HtmlTableRow();
		//
		result.add(datas);
		//
		return result;
	}

	public static HtmlTableRow data(Iterable<HtmlTableData> datas) {
		return data(StreamUtils.from(datas).toArray(HtmlTableData[]::new));
	}

	public static HtmlTableRow header(HtmlTableHeader... headers) {
		HtmlTableRow result = new HtmlTableRow();
		//
		result.add(headers);
		//
		return result;
	}

	public static HtmlTableRow header(Iterable<HtmlTableHeader> headers) {
		return header(StreamUtils.from(headers).toArray(HtmlTableHeader[]::new));
	}
}
