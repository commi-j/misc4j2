package tk.labyrinth.misc4j2.lib.vaadin.tool.setup;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.AfterNavigationListener;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.BeforeLeaveListener;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.spring.scopes.VaadinUIScope;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import tk.labyrinth.misc4j2.java.lang.reflect.ClassUtils;
import tk.labyrinth.misc4j2.lib.spring.meta.LazyComponent;

import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.1
 */
@LazyComponent
@Slf4j
public class UiScopedBeansInitializer implements BeanFactoryAware, UIInitListener {

	private static final List<Class<?>> supportedUiListenerClasses = List.of(
			AfterNavigationListener.class,
			BeforeEnterListener.class,
			BeforeLeaveListener.class);

	private List<ObjectProvider<?>> listenerProviders;

	private void initialize(ConfigurableListableBeanFactory beanFactory) {
		val uiScopedBeanDefinitions = List.of(beanFactory.getBeanDefinitionNames())
				.map(beanFactory::getBeanDefinition)
				.filter(beanDefinition ->
						Objects.equals(beanDefinition.getScope(), VaadinUIScope.VAADIN_UI_SCOPE_NAME) ||
								ClassUtils.get(beanDefinition.getBeanClassName())
										.isAnnotationPresent(RegisterOnUiInit.class))
				.map(beanDefinition -> {
					Class<?> beanClass = ClassUtils.get(beanDefinition.getBeanClassName());
					//
					return new Tuple3<>(beanDefinition, beanClass, beanFactory.getBeanProvider(beanClass));
				});
		//
		listenerProviders = uiScopedBeanDefinitions
				.filter(tuple -> supportedUiListenerClasses.exists(
						supportedUiListenerClass -> supportedUiListenerClass.isAssignableFrom(tuple._2())))
				.filter(tuple -> {
					boolean result;
					{
						if (Objects.equals(tuple._1().getScope(), VaadinUIScope.VAADIN_UI_SCOPE_NAME) &&
								tuple._1().isLazyInit()) {
							// We do not initialize lazy beans, so such combination looks like misconfiguration.
							logger.warn(
									"Detected UI Scoped Bean which is eligible listener but also Lazy: className = {}",
									tuple._2().getName());
							//
							result = false;
						} else {
							result = true;
						}
					}
					return result;
				})
				.map(Tuple3::_3);
	}

	@Override
	public void setBeanFactory(@NonNull BeanFactory beanFactory) throws BeansException {
		initialize((ConfigurableListableBeanFactory) beanFactory);
	}

	@Override
	public void uiInit(UIInitEvent event) {
		UI ui = event.getUI();
		//
		logger.debug("Initializing UI with id = {}", ui.getUIId());
		//
		ui.accessSynchronously(() -> listenerProviders
				.map(ObjectProvider::getObject)
				.forEach(listener -> {
					if (listener instanceof AfterNavigationListener afterNavigationListener) {
						logger.debug("Registering AfterNavigationListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addAfterNavigationListener(afterNavigationListener);
					}
					if (listener instanceof BeforeEnterListener beforeEnterListener) {
						logger.debug("Registering BeforeEnterListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addBeforeEnterListener(beforeEnterListener);
					}
					if (listener instanceof BeforeLeaveListener beforeLeaveListener) {
						logger.debug("Registering BeforeLeaveListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addBeforeLeaveListener(beforeLeaveListener);
					}
				}));
	}
}
