package tk.labyrinth.misc4j2.lib.jackson;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import tk.labyrinth.misc4j2.common.configurer.ObjectConfigurer;

import java.util.List;

/**
 * TODO: Create examples of bad behaviour without these settings.<br>
 * <br>
 * Dependencies:<br>
 * - com.fasterxml.jackson.core:jackson-databind;<br>
 * - com.fasterxml.jackson.datatype:jackson-datatype-jsr310;<br>
 *
 * @author Commitman
 * @version 1.0.7
 */
public class ObjectMapperFactory {

	public static ObjectMapper defaultConfigured() {
		return defaultConfigured(List.of());
	}

	public static ObjectMapper defaultConfigured(List<ObjectMapperConfigurer> configurers) {
		ObjectMapper baseObjectMapper = new ObjectMapper();
		{
			// Handle fields and creators.
			// Ignore methods.
			baseObjectMapper.setDefaultVisibility(JsonAutoDetect.Value.construct(
					JsonAutoDetect.Visibility.ANY,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.ANY));
		}
		{
			// Avoiding jsons being overpopulated with null values.
			baseObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		}
		{
			// No reason to fail if serialized model is empty, it can be populated later.
			baseObjectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		}
		{
			// Using human-friendly formats for date, time & durations.
			baseObjectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
			baseObjectMapper.disable(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS);
		}
		{
			// https://github.com/FasterXML/jackson-modules-java8/tree/master/datetime
			// Must be manually enabled in 2.x <= x < 3.x versions.
			// You can try to remove it if you use 3.x version.
			baseObjectMapper.registerModule(new JavaTimeModule());
		}
		{
			// https://github.com/FasterXML/jackson-databind/issues/585
			// Ensures Jackson pretty prints with "\n" instead of System.getProperty("line.separator")
			// to keep consistent platform-independent behaviour.
			// The problem was initially found in tests comparing prettified jsons with textblock Strings
			// as latters always use "\n".
			baseObjectMapper.setDefaultPrettyPrinter(
					new DefaultPrettyPrinter().withObjectIndenter(
							new DefaultIndenter().withLinefeed("\n")));
		}
		//
		ObjectMapper configuredObjectMapper = ObjectConfigurer.configure(baseObjectMapper, configurers);
		//
		return configuredObjectMapper;
	}

	public static ObjectMapper defaultConfigured(ObjectMapperConfigurer... configurers) {
		return defaultConfigured(List.of(configurers));
	}
}
