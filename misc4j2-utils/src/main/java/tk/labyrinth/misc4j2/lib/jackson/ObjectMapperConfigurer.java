package tk.labyrinth.misc4j2.lib.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import tk.labyrinth.misc4j2.common.configurer.ObjectConfigurer;

/**
 * @author Commitman
 * @version 1.0.0
 */
public interface ObjectMapperConfigurer extends ObjectConfigurer<ObjectMapper> {

	@Override
	ObjectMapper configure(ObjectMapper objectMapper);
}
