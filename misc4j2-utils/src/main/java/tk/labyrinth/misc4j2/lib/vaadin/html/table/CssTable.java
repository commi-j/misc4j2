package tk.labyrinth.misc4j2.lib.vaadin.html.table;

import com.vaadin.flow.component.HasElement;
import tk.labyrinth.misc4j2.lib.vaadin.css.BorderCollapse;
import tk.labyrinth.misc4j2.lib.vaadin.css.CustomCssProperty;
import tk.labyrinth.misc4j2.lib.vaadin.dom.StyleUtils;

/**
 * @author Commitman
 * @version 1.0.0
 */
public interface CssTable<T extends CssTable<T>> extends HasElement {

	@SuppressWarnings("unchecked")
	default T setBorderCollapse(BorderCollapse borderCollapse) {
		StyleUtils.setCssProperty(this, borderCollapse);
		//
		return (T) this;
	}

	@SuppressWarnings("unchecked")
	default T setBorderSpacing(String borderSpacing) {
		StyleUtils.setCssProperty(this, CustomCssProperty.of("border-spacing", borderSpacing));
		//
		return (T) this;
	}
}
