package tk.labyrinth.misc4j2.lib.spring.context.event;

import io.micrometer.core.aop.CountedAspect;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * For Spring AOP this one should be context bean.
 *
 * @author Commitman
 * @version 1.0.1
 * @see CountedAspect
 * @see TimedAspect
 */
@Aspect
@Component
public class MetricsReportingEventListenerAspect {

	public static final String BASE_METRIC_NAME = "spring-event-listener-execution";

	public static final String EMPTY_TAG_VALUE = "_EMPTY";

	public static final String FAULT_TAG_NAME = "fault";

	public static final String METHOD_SIGNATURE_TAG_NAME = "methodSignature";

	@Around("@annotation(org.springframework.context.event.EventListener)")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		Tag methodSignatureTag = Tag.of(METHOD_SIGNATURE_TAG_NAME, createSignature(joinPoint));
		//
		Metrics.counter(BASE_METRIC_NAME, List.of(methodSignatureTag)).increment();
		Timer.Sample sample = Timer.start();
		//
		Throwable fault = null;
		try {
			return joinPoint.proceed();
		} catch (Throwable t) {
			fault = t;
			throw t;
		} finally {
			Tag faultTag = Tag.of(FAULT_TAG_NAME, fault != null ? fault.getClass().getName() : EMPTY_TAG_VALUE);
			sample.stop(Metrics.timer(BASE_METRIC_NAME, List.of(methodSignatureTag, faultTag)));
		}
	}

	public static String createSignature(ProceedingJoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		return String.format(
				"%s#%s(%s)",
				signature.getDeclaringTypeName(),
				signature.getName(),
				Stream.of(signature.getParameterTypes())
						.map(Class::getName)
						.collect(Collectors.joining(",")));
	}
}
