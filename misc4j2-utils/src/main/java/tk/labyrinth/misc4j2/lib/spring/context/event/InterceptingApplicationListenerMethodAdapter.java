package tk.labyrinth.misc4j2.lib.spring.context.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ApplicationListenerMethodAdapter;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;

public class InterceptingApplicationListenerMethodAdapter extends ApplicationListenerMethodAdapter {

	private final MethodHandle shouldHandleHandle;

	/**
	 * Construct a new ApplicationListenerMethodAdapter.
	 *
	 * @param beanName    the name of the bean to invoke the listener method on
	 * @param targetClass the target class that the method is declared on
	 * @param method      the listener method to invoke
	 */
	public InterceptingApplicationListenerMethodAdapter(String beanName, Class<?> targetClass, Method method) {
		super(beanName, targetClass, method);
		try {
			shouldHandleHandle = MethodHandles.lookup()
					.findVirtual(
							ApplicationListenerMethodAdapter.class,
							"shouldHandle",
							MethodType.methodType(boolean.class, ApplicationEvent.class, Object[].class))
					.bindTo(this);
		} catch (ReflectiveOperationException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void processEvent(ApplicationEvent event) {
		Object[] args = resolveArguments(event);
		//
		boolean shouldHandle;
		try {
			shouldHandle = (boolean) shouldHandleHandle.invokeExact(event, args);
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
		//
		if (shouldHandle) {
			Object result = doInvoke(args);
			if (result != null) {
				handleResult(result);
			} else {
				logger.trace("No result object given - no result to handle");
			}
		}
	}
}
