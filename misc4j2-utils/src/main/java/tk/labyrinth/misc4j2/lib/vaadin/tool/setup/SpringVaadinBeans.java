package tk.labyrinth.misc4j2.lib.vaadin.tool.setup;

import com.vaadin.flow.server.VaadinServletContext;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import tk.labyrinth.misc4j2.lib.spring.meta.LazyConfiguration;
import tk.labyrinth.misc4j2.lib.spring.meta.PrototypeScopedBean;

import javax.servlet.ServletContext;

/**
 * @author Commitman
 * @version 1.0.0
 */
@LazyConfiguration
public class SpringVaadinBeans {

	@PrototypeScopedBean
	public static ApplicationRouteRegistry applicationRouteRegistry(ServletContext servletContext) {
		return ApplicationRouteRegistry.getInstance(servletContext);
	}

	@PrototypeScopedBean
	public static VaadinServletContext vaadinServletContext(ServletContext servletContext) {
		return new VaadinServletContext(servletContext);
	}
}
