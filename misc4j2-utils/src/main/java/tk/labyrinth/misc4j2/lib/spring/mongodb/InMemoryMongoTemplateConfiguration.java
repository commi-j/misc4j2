package tk.labyrinth.misc4j2.lib.spring.mongodb;

import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class InMemoryMongoTemplateConfiguration {

	@Bean
	public static MongoTemplate inMemoryMongoTemplate() {
		MongoServer server = new MongoServer(new MemoryBackend());
		MongoClient client = MongoClients.create("mongodb://" + new ServerAddress(server.bind()));
		return new MongoTemplate(client, "testdb");
	}
}
