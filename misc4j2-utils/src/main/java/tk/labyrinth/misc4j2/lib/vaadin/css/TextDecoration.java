package tk.labyrinth.misc4j2.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/Css/css_text_decoration.asp">https://www.w3schools.com/Css/css_text_decoration.asp</a>
 *
 * @author Commitman
 * @version 1.0.2
 * @see TextDecorationLine
 */
@Value(staticConstructor = "of")
public class TextDecoration implements CssProperty {

	public static final TextDecoration UNDERLINE = of("underline");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "text-decoration";
	}
}
