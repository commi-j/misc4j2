package tk.labyrinth.misc4j2.lib.poi;

import org.apache.poi.ss.usermodel.Cell;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class CellUtils {

	/**
	 * @param cell non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getValueAsString(Cell cell) {
		String result;
		switch (cell.getCellType()) {
			case NUMERIC:
				result = Double.toString(cell.getNumericCellValue());
				break;
			case STRING:
				result = cell.getStringCellValue();
				break;
			default:
				throw new UnsupportedOperationException(ExceptionUtils.render(cell));
		}
		return result;
	}
}
