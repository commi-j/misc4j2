package tk.labyrinth.misc4j2.lib.commons.lang3.tuple;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class PairUtils {

	/**
	 * @param pair  non-null
	 * @param value nullable
	 * @param <L>   <b>L</b>eft of Pair to be <b>L</b>eft of Triple
	 * @param <R>   <b>R</b>ight of Pair to be <b>M</b>iddle of Triple
	 * @param <V>   <b>V</b>alue to be <b>R</b>ight of Triple
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <L, R, V> Triple<L, R, V> append(Pair<L, R> pair, @Nullable V value) {
		return Triple.of(pair.getLeft(), pair.getRight(), value);
	}

	/**
	 * @param pair  non-null
	 * @param value nullable
	 * @param <L>   <b>L</b>eft of Pair to be <b>M</b>iddle of Triple
	 * @param <R>   <b>R</b>ight of Pair to be <b>R</b>ight of Triple
	 * @param <V>   <b>V</b>alue to be <b>L</b>eft of Triple
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <L, R, V> Triple<V, L, R> prepend(Pair<L, R> pair, @Nullable V value) {
		return Triple.of(value, pair.getLeft(), pair.getRight());
	}
}
