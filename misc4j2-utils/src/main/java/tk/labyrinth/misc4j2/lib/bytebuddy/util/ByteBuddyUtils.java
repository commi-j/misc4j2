package tk.labyrinth.misc4j2.lib.bytebuddy.util;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.ClassFileVersion;
import net.bytebuddy.NamingStrategy;
import net.bytebuddy.description.type.TypeDescription;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ByteBuddyUtils {

	private static final NamingStrategy defaultNamingStrategy = new ParameterizedChildDefaultNamingStrategy();

	@SuppressWarnings("unchecked")
	public static <C> Class<? extends C> generateParameterizedChild(Class<C> baseType, Class<?>... parameterTypes) {
		return (Class<? extends C>) new ByteBuddy(ClassFileVersion.JAVA_V8)
				.with(defaultNamingStrategy)
				.subclass(TypeDescription.Generic.Builder.parameterizedType(baseType, parameterTypes).build())
				.make()
				.load(Thread.currentThread().getContextClassLoader())
				.getLoaded();
	}
}
