package tk.labyrinth.misc4j2.lib.vaadin.layout.cssgrid;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CssGridTemplateAreas {

	/**
	 * {@code Map<RowIndex,Map<ColumnIndex,Area>>}
	 */
	private final SortedMap<Integer, SortedMap<Integer, String>> cells = new TreeMap<>();

	public CssGridTemplateAreas cell(@Nullable String area, int rowIndex, int columnIndex) {
		return cell(area, rowIndex, columnIndex, 1, 1);
	}

	public CssGridTemplateAreas cell(@Nullable String area, int rowIndex, int columnIndex, int rowSpan, int columnSpan) {
		for (int i = 0; i < rowSpan; i++) {
			for (int j = 0; j < columnSpan; j++) {
				cells.computeIfAbsent(rowIndex + i, key -> new TreeMap<>()).put(columnIndex + j, area);
			}
		}
		return this;
	}

	public String[] render() {
		int rowCount = cells.keySet().stream()
				.mapToInt(Integer::intValue)
				.max().orElse(-1) + 1;
		int columnCount = cells.values().stream()
				.map(Map::keySet)
				.flatMap(Collection::stream)
				.mapToInt(Integer::intValue)
				.max().orElse(-1) + 1;
		String[] rows = new String[rowCount];
		for (int i = 0; i < rowCount; i++) {
			SortedMap<Integer, String> row = cells.getOrDefault(i, Collections.emptySortedMap());
			rows[i] = IntStream.range(0, columnCount)
					.mapToObj(j -> row.getOrDefault(j, "."))
					.collect(Collectors.joining(" "));
		}
		return rows;
	}
}
