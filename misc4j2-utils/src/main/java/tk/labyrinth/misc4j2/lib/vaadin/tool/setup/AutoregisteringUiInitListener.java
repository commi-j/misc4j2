package tk.labyrinth.misc4j2.lib.vaadin.tool.setup;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;

/**
 * {@link UIInitListener} that listens for {@link ServiceInitEvent}<br>
 * and registers itself with {@link VaadinService#addUIInitListener(UIInitListener)}.<br>
 *
 * @deprecated Vaadin learned to do it on its own.
 */
@Deprecated
public interface AutoregisteringUiInitListener extends UIInitListener, VaadinServiceInitListener {

	@Override
	default void serviceInit(ServiceInitEvent event) {
		event.getSource().addUIInitListener(this);
	}
}
