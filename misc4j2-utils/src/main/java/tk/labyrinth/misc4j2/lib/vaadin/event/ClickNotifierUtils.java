package tk.labyrinth.misc4j2.lib.vaadin.event;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.shared.Registration;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ClickNotifierUtils {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static <C extends Component> Registration addClickListener(
			C component,
			ComponentEventListener<ClickEvent<C>> listener) {
		return ComponentUtil.addListener(component, ClickEvent.class, (ComponentEventListener) listener);
	}
}
