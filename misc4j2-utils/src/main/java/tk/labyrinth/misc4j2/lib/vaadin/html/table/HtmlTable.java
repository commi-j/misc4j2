package tk.labyrinth.misc4j2.lib.vaadin.html.table;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Tag("table")
public class HtmlTable extends HtmlContainer implements CssTable<HtmlTable> {

	public HtmlTable() {
		// no-op
	}

	public HtmlTable(HtmlTableRow... rows) {
		super(rows);
	}

	public HtmlTable(Iterable<HtmlTableRow> rows) {
		super(StreamUtils.from(rows).toArray(HtmlTableRow[]::new));
	}
}
