package tk.labyrinth.misc4j2.lib.vaadin.layout.cssgrid.extra;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.router.RouterLayout;
import tk.labyrinth.misc4j2.lib.vaadin.layout.cssgrid.CssGridAware;

public interface CssGridRouterLayout extends RouterLayout {

	String getContentGridAreaName();

	CssGridAware getLayout();

	@Override
	default void showRouterLayoutContent(HasElement content) {
		if (content != null) {
			getLayout().add((Component) content, getContentGridAreaName());
		}
	}
}
