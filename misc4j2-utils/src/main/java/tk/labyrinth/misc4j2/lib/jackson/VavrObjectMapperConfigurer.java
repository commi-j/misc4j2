package tk.labyrinth.misc4j2.lib.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.jackson.datatype.VavrModule;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class VavrObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			// https://github.com/vavr-io/vavr-jackson
			objectMapper.registerModule(new VavrModule());
		}
		return objectMapper;
	}
}
