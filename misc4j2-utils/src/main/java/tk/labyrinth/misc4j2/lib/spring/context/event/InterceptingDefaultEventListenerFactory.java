package tk.labyrinth.misc4j2.lib.spring.context.event;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.DefaultEventListenerFactory;

import java.lang.reflect.Method;

public class InterceptingDefaultEventListenerFactory extends DefaultEventListenerFactory {

	@Override
	public ApplicationListener<?> createApplicationListener(String beanName, Class<?> type, Method method) {
		return new InterceptingApplicationListenerMethodAdapter(beanName, type, method);
	}
}
