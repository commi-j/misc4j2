package tk.labyrinth.misc4j2.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csSref/pr_border-collapse.php">https://www.w3schools.com/csSref/pr_border-collapse.php</a>
 *
 * @author Commitman
 * @version 1.1.1
 */
@Value(staticConstructor = "of")
public class BorderCollapse implements CssProperty {

	/**
	 * @since 1.1.0
	 */
	public static final BorderCollapse COLLAPSE = of("collapse");

	/**
	 * @since 1.1.0
	 */
	public static final BorderCollapse INHERIT = of("inherit");

	/**
	 * @since 1.1.0
	 */
	public static final BorderCollapse INITIAL = of("initial");

	/**
	 * @since 1.1.0
	 */
	public static final BorderCollapse NOT_SPECIFIED = of(null);

	/**
	 * @since 1.1.0
	 */
	public static final BorderCollapse SEPARATE = of("separate");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "border-collapse";
	}
}
