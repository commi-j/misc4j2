package tk.labyrinth.misc4j2.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#COLUMN}).<br>
 * Note: this property may be altered later, even to "row" values.<br>
 */
public class CssVerticalLayout extends CssFlexboxLayout {

	public CssVerticalLayout() {
		super(FlexDirection.COLUMN);
	}

	public CssVerticalLayout(Component... components) {
		this();
		add(components);
	}
}
