package tk.labyrinth.misc4j2.javax.xml.stream;

import org.apache.commons.io.output.StringBuilderWriter;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class XmlStreamUtils {

	/**
	 * Advances cursor of provided xmlEventReader to the first occurence of element with elementName.
	 *
	 * @param xmlEventReader non-null
	 * @param elementName    non-empty
	 *
	 * @return true if found, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean findNextElementWithName(XMLEventReader xmlEventReader, String elementName) {
		boolean found = false;
		while (xmlEventReader.hasNext()) {
			try {
				XMLEvent xmlEvent = xmlEventReader.peek();
				if (xmlEvent.isStartElement() &&
						Objects.equals(xmlEvent.asStartElement().getName().getLocalPart(), elementName)) {
					found = true;
					break;
				}
				xmlEventReader.nextEvent();
			} catch (XMLStreamException ex) {
				throw new RuntimeException(ex);
			}
		}
		return found;
	}

	/**
	 * Reads out element at xmlEventReader's cursor into a String.
	 *
	 * @param xmlEventReader non-null
	 *
	 * @return non-empty
	 *
	 * @throws IllegalArgumentException if xmlEventReader's cursor is not at startElement
	 * @since 1.0.0
	 */
	public static String readNextElementAsString(XMLEventReader xmlEventReader) {
		StringBuilderWriter stringBuilderWriter = new StringBuilderWriter();
		//
		try {
			XMLEvent firstEvent = xmlEventReader.peek();
			if (firstEvent == null || !firstEvent.isStartElement()) {
				throw new IllegalArgumentException("Require firstEvent to be startElement: %s".formatted(firstEvent));
			}
			//
			int depth = 0;
			do {
				XMLEvent nextEvent = xmlEventReader.nextEvent();
				nextEvent.writeAsEncodedUnicode(stringBuilderWriter);
				//
				if (nextEvent.isStartElement()) {
					depth++;
				}
				if (nextEvent.isEndElement()) {
					depth--;
				}
			} while (depth > 0);
		} catch (XMLStreamException ex) {
			throw new RuntimeException(ex);
		}
		//
		return stringBuilderWriter.toString();
	}
}
