package tk.labyrinth.misc4j2.lang;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public class SimpleAccessor<T> implements Accessor<T> {

	private T value;

	public SimpleAccessor() {
		this(null);
	}

	public SimpleAccessor(T value) {
		this.value = value;
	}

	@Override
	public T get() {
		return value;
	}

	@Override
	public T set(T value) {
		T previousValue = this.value;
		this.value = value;
		return previousValue;
	}
}
