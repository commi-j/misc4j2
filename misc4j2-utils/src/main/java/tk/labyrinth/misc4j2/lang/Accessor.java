package tk.labyrinth.misc4j2.lang;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface Accessor<T> {

	T get();

	T set(T value);
}
